# 1169  - Pingren

# County Title
title = c_pingren

# Settlements
max_settlements = 4

b_pingren_castle = castle
b_pingren_city1 = city
b_pingren_temple = temple
b_pingren_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_pingren_castle = ca_asoiaf_yiti_basevalue_1
	b_pingren_castle = ca_asoiaf_yiti_basevalue_2

	b_pingren_city1 = ct_asoiaf_yiti_basevalue_1

}
	