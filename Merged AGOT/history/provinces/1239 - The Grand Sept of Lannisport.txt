

# County Title
title = c_mbs_lannisport_sept

# Settlements
max_settlements = 1
b_mbs_lannisport_sept = temple

# Misc
culture = old_first_man
religion = old_gods

# History
1.1.1 = {
	
	b_mbs_lannisport_sept = tp_monastery_1

}
6700.1.1 = {
	culture = westerman
	religion = the_seven
}