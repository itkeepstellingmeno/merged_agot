# 1206  - Jinmai

# County Title
title = c_jinmai

# Settlements
max_settlements = 4

b_jinmai_castle = castle
b_jinmai_city1 = city
b_jinmai_temple = temple
b_jinmai_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jinmai_castle = ca_asoiaf_yiti_basevalue_1
	b_jinmai_castle = ca_asoiaf_yiti_basevalue_2

	b_jinmai_city1 = ct_asoiaf_yiti_basevalue_1
	b_jinmai_city1 = ct_asoiaf_yiti_basevalue_2

}
	