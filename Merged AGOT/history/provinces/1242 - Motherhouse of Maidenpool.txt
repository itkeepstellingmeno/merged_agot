
# County Title
title = c_mbs_maidenpool_motherhouse

# Settlements
max_settlements = 2
b_mbs_maidenpool_motherhouse = temple
b_mbs_maidenpool_motherhouse2 = city

# Misc
culture = old_first_man
religion = old_gods


# History

1.1.1 = {

	b_mbs_maidenpool_motherhouse = tp_monastery_1
}
6700.1.1 = {
	culture = riverlander
	religion = the_seven
}