# 117 - Isle of Faces

# County Title
title = c_isleoffaces

# Settlements
max_settlements = 1
b_isleoffaces = temple

# Misc
culture = children_forest
religion = old_gods

# History
1.1.1 = {
	b_isleoffaces = tp_weirwood_tree
	b_isleoffaces = tp_small_godswood
	b_isleoffaces = tp_godswood
	b_isleoffaces = tp_large_godswood
	b_isleoffaces = tp_godswood_forest
}