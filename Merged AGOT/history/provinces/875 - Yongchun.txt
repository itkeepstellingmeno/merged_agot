# 875  - Yongchun

# County Title
title = c_yongchun

# Settlements
max_settlements = 7

b_wudong_castle = castle
b_yongchun_city1 = city
b_yongchun_temple = temple
b_yongchun_city2 = city
b_yongchun_castle1 = castle
b_yongchun_castle2 = city
b_yongchun_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_wudong_castle = ca_asoiaf_yiti_basevalue_1
	b_wudong_castle = ca_asoiaf_yiti_basevalue_2
	b_wudong_castle = ca_asoiaf_yiti_basevalue_3
	b_wudong_castle = ca_asoiaf_yiti_basevalue_4
	b_wudong_castle = ca_wudong_castle

	b_yongchun_city1 = ct_asoiaf_yiti_basevalue_1
	b_yongchun_city1 = ct_asoiaf_yiti_basevalue_2
	b_yongchun_city1 = ct_asoiaf_yiti_basevalue_3

	b_yongchun_city2 = ct_asoiaf_yiti_basevalue_1
	b_yongchun_city2 = ct_asoiaf_yiti_basevalue_2
	b_yongchun_city2 = ct_asoiaf_yiti_basevalue_3

	b_yongchun_castle1 = ca_asoiaf_yiti_basevalue_1

	b_yongchun_castle2 = ct_asoiaf_yiti_basevalue_1

	b_yongchun_castle3 = ca_asoiaf_yiti_basevalue_1

}
	