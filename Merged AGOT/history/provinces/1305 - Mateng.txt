# 1208  - Mateng

# County Title
title = c_mateng

# Settlements
max_settlements = 4

b_mateng_castle = castle
b_mateng_city1 = city
b_mateng_temple = temple
b_mateng_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_mateng_castle = ca_asoiaf_yiti_basevalue_1
	b_mateng_castle = ca_asoiaf_yiti_basevalue_2

	b_mateng_city1 = ct_asoiaf_yiti_basevalue_1
	b_mateng_city1 = ct_asoiaf_yiti_basevalue_2

}
	