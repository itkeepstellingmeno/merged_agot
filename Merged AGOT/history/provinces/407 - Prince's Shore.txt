# 408 - Prince's Shore

# County Title
title = c_princesshore

# Settlements
max_settlements = 3

b_princesshore_castle = castle
b_princesshore = city
b_princesshore_temple = temple

# Misc
culture = old_andal
religion = the_seven

# History
1.1.1 = {
	b_princesshore = ct_asoiaf_essos_basevalue_1
	b_princesshore = ct_asoiaf_essos_basevalue_2
	b_princesshore = ct_asoiaf_essos_basevalue_3
	
	b_princesshore_castle = ca_asoiaf_essos_basevalue_1
	b_princesshore_castle = ca_asoiaf_essos_basevalue_2
	
	trade_post = b_thelis
}
4000.1.1 = {
	culture = pentosi
	religion = rhllor
	capital = b_princesshore
}