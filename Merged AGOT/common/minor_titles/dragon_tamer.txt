title_dragon_tamer = {
	dignity = 0.25
	grant_limit = 99
	show_as_title = no
	revoke_allowed = yes
	opinion_effect = 10
	monthly_prestige = 0.05

	allowed_to_grant = {
		OR = {
			primary_title = {
				OR = {
					real_tier = KING
					real_tier = EMPEROR
				}
				has_law = dragon_tamer_1
			}
			AND = {
				independent = yes
				NOT = { has_any_opinion_modifier = opinion_de_facto_liege }
				primary_title = {
					real_tier = KING
					has_law = dragon_tamer_1
				}
			}
		}
	}
	
	allowed_to_hold = {
		OR = {
			trait = dragon_egg
			any_friend = {
				trait = dragon
			}
		}
	}

	gain_effect = { }
	lose_effect = { }

	message = no
}