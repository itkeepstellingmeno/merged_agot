k_the_biteTK = {
	color = { 217 184 69 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_bite }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 124 # Schorched Vale
	creation_requires_capital = no
	culture = moon_clansman
}

k_eastwealdTK = {
	color = { 202 242 202 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_eastweald }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 139 # Longbow Hall
	creation_requires_capital = no
	culture = valeman
}

k_the_fingersTK = {
	color = { 188 16 14 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_fingers }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 132 # Sunset Keep
	creation_requires_capital = no
	culture = valeman
}

k_the_giants_lanceTK = {
	color = { 8 184 255 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_giants_lance }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 136 # The Eyrie
	creation_requires_capital = no
	culture = valeman
}

k_gulltownTK = {
	color={ 33 119 144 }
	color2={ 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_gulltown }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 149 #Gulltown
	creation_requires_capital = no
	culture = valeman
}

k_ironoaksTK = {
	color = { 80 208 200 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_ironoaks }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 142 # Ironoaks
	creation_requires_capital = yes
	culture = valeman
}

k_northwealdTK = {
	color = { 226 194 194 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_northweald }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 130 # Newkeep
	creation_requires_capital = yes
	culture = valeman
}

k_redfortTK = {
	color = { 235 15 15 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_redfort }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 144 #Redfort
	creation_requires_capital = yes
	culture = valeman

	title_prefix = "prefix_kingdomofthe"
}

k_runestoneTK = {
	color = { 228 109 37 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_runestone }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	culture = valeman
	capital = 150 # Runestone
	creation_requires_capital = yes

	# Runic Crown of Runestone
	title_prefix = "prefix_runiccrownof"
	title = "bronzeking_male"
	title_female = "bronzeking_female"
}

k_the_sistersTK = {
	color = { 16 169 176 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_the_sisters }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 119 # Sweetsister
	creation_requires_capital = no
	culture = sisterman
}

k_strongsongTK = {
	color = { 95 122 117 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_strongsong }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 133 # Strongsong
	creation_requires_capital = no
	culture = valeman
}

k_wickendenTK = {
	color = { 40 202 204 }
	color2 = { 255 255 255 }

	allow = {
		num_of_duke_titles = 2
		primary_title = { title = d_wickenden }
		custom_tooltip = {
			text = TOOLTIPINDEPENDENTCONDITION
			hidden_tooltip = {
				independent = yes
				primary_title = { NOT = { check_variable = { which = "de_facto_empire" value = 1 } } }
			}
		}
	}

	capital = 147 # Wickenden
	creation_requires_capital = yes
	culture = valeman
}