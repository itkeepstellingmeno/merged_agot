valyrian = {
	graphical_cultures = { westerngfx } # buildings, portraits, units
	
	high_valyrian = {
		graphical_cultures = { valyriangfx } # portraits
		
		used_for_random = no
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		character_modifier = {
			dragon_hatching = 4
			dragon_taming = 30
		}
		
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			Aerys:400 Aegon:1100 Aegor Aelyx Aemon:400 Aemond Aenar Aenys:200 Aerion:300 Aeryn Aelor Aurion:50 Aethon:50 Aenyx:25
			Baelor:200 Baelon:300 Baethon:50 Baenar:50 Baeryn:50 Balaeron:25 Balerion:5
			Daegar:50 Daemon:400 Daeron:600 Daemion Daenar:50
			Gaemon:200 Haegon Haerys:50
			Jaekar:50 Jaehaerys:400 Jacaerys Jaenar:50 Jaenyx:25
			Lucerys Laenor Laerion:25
			Maelys Maegon Maegor:200 Maekar Maerys:50 Matarys Maelor
			Orys:50
			Rhaegar Rhaegel Rhogar
			Taegon:25
			Valarr Viserys:400 Vaeron Vaekar:50 Vaermon Vaemond:50 Vaegon Valerion Valyx:25
		}
		female_names = {
			Allyria:50 Alearys:50 Aelinor Alysanne Aerea Alyssa Aelora
			Baela Baenys:50 Baena:50
			Cyeana Cymella:50 Calla		
			Daella:200 Daena Daeoril Daenerys:200 Daenys Daenaera Daenora
			Elaena:200
			Gael
			Helaena Haera 
			Jaehaera Jaenara
			Leaysa:50 Laena Lianna Larissa
			Maera:50 Maega:50 Maegelle
			Naerys Rhae Rhaelle Rhaena:300 Rhaeys:50 Rhaenyra Rhaelinor:50 Rhaenys:300 Rhaera:50
			Saenrys:50 Syaella:50 Shaera Saera Shaena
			Visenya:200 Vaera:50 Vaella:200 Viserra Vaena:50
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 15
		mat_grf_name_chance = 5
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 5
		mother_name_chance = 0
		
		disinherit_from_blinding = yes
		
		modifier = default_culture_modifier
	}
	
	western_valyrian = {
		# Westerosi Valyrian
		graphical_cultures = { westvalyriangfx } # portraits
		
		color = { 0.35 0.35 0.35 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		character_modifier = {
			dragon_hatching = 1
			dragon_taming = 4
		}
		
		male_names = {
			Aegon Aegor Aelix Aemon Aemond Aenys Aerion Aerys Alan Alaric Allar Allard Alton Alyn Ardrian Arryk Aurane Arthor Addam Aethan
			Baelor Balman Bartimos Bennard Boros Bryen
			Clarence Clayton Clement Crispian Corlys Corwyn
			Daemon Daeron Danos Davos Denys Dontos Duncan Duram
			Edwell Florian
			Guncer Gyles Galfrid Gargon
			Haegon Hubard
			Jacelyn Jaehaerys Janos Jarman Jarmen Jon Jorgen Justin Jacaerys Joffrey
			Lothar Lothor Lucos Lucerys Laenor Laemond:50
			Maegor Maekar Malentine Malliard Maric Matarys Matthos Monford Monterys Morgan Morros
			Ollidor Orys
			Pate
			Qarlton Quenton
			Rhaegar Rhaegel Rolland
			Sefton Simon Steffon Symon Symeon
			Triston
			Ulmer
			Valarr Viserys Vaermon Victor Vaemond Vaellyn
		}
		female_names = {
			Aelinor Aglantine Alerie Alla Alyce Alynne Alys Alysanne Annara Arianne Alynna:50 Aenesa:50
			Baena:50	
			Cassana Corenna Cyrenna
			Dalla Delena Denyse Desmera Donyse Daella Daena Daenerys Daeryssa Daesanne:50
			Eglantine Elaena Ermesande
			Falena Falyse
			Jeyne Jonquil Joyeuse
			Leona Leonette Leyla Larissa Lianna Leana
			Malessa Malora Marei Marya Matrice Melicent Mhaegan Mhaegen Moelle Mylenda Marilda
			Prudence Prunella
			Rhae Rhaella Rhaelle Rhaena Rhaenyra Rhaenys Roelle Rhaera:50
			Scolera Selyse Senelle Serenei Shiera Shireen
			Talla Tanda
			Unella
			Visenya Vaena:50
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 10
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
	
	eastern_valyrian = {
		# Essosi Valyrian
		graphical_cultures = { valyriangfx republicsgfx }
		
		color = { 0.25 0.25 0.25 }
		
		character_modifier = {
			dragon_hatching = 1
			dragon_taming = 4
		}
		
		used_for_random = no
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			#CANON
			
			Alios:450
			Belicho:450
			Colloquo:450
			Doniphos:450
			Horonno:1000
			Malaquo:450 Methyso:250 Marqelo:450
			Nyessos:450
			Parquello:450
			Vogarro:450
			Kasporio:150
			Tybero:150
			Mollono:200
			Vhalaso:700
				
			Asporio:150 Adaros Arano Aeriphos Arillos Aryrio Aryos Aeryio Aricho:250 Aerano Aerio Aronos Adareno Aeryro
			Bracharo Basporio:150 Belesso Ballio Balleno Brachyllo Belodos Brachanos Beleqor:150 Belyrio Belapho Beloquo Belano Belono Boronno:200 Bhalaso:200
			Conicho Cassano Cadaros Caryos Cossoquo:250 Coronno:150 Carios:150 Cybero:250 Coqorro:150 
			Donicho:150 Donelos Donano Draqano Donio Donaro Draqoros Dynario Doronno:150 Davo:150 Darios:150
			Esselo Ezzano Ezzario Essanos Essalos Essorio Eysano Esyrno 
			Harlano:250 Hasporio:150 Haronno:250 Horio:250 Harios:150 Hybario:200 Halaso:150
			Innyllo Irrio Illeno Illario Illio Innario Illoquo Illaro Illoros Ironno:200 Illonar Irricho
			Jasporio:150 Joraquo:150 Jorapho:150 Jaqaro Jalaquo:150 Jaqario Joronno:150 Jybero Jhalaso
			Gasporio:150 Goranys Gyllonos Garridos Goronno:150 Garios:150 Goqorro:150
			Lysiros Lazano Lysenno Larazo Laraquo:150 Loronno:250 Larios:150 Lybero:150 Lothorio:150 Loqorro:150
			Noroquo Nakano Nakaquo Nakodos Norello Nyessano Nakeo Narios:150
			Maraphos:250 Mallaquo:250 Marano Malano Malonaro Malero:250 Melio Moronno:150 Noqorro:150 Mhalaso:150
			Paranys Pazano Pyrio Padaro Poronno:150 Parios:150 Poqorro:150 Phalaso:150
			Releqor:250 Relyrio Relapho:250 Rybero:150 Rhalaso:150 Rhogar:150
			Qarrano:250 Qorello Qoronno:150 Qhalaso:200
			Yasporio:150  Yaqeo Yorys:150 Yoronno:150 Yoqorro:150 
			Stallaquo:250 Stallorno Syrano Stalleno Syricho:250 Salloros Soronno:150 Sarios:150 Sybero:150
			Tregaquo Tregyros Tregonos Thoronno Thorio Tyssano Tychano Thoreno Tychoros Tregyllo Toronno:150 Tario:150 Thalaso:150 Tazal
			Vargorno Variphos:250 Vyronos Vargano:250 Vargio Vyresso Voqorro:250 Vogorno:250 Valoquo:250 Vargello Voronno:150 Vario:150 
			Zonario Zarapho Zaraquo Zyrio Zeleqor Zargano Zoronno:150 Zarios:150 Zhalaso:150
			
			Pupolo Danilo Demetio Nereo Balderico Genesio	
		}
		female_names = {
			
			Allyria Alearys
			Cyeana Cymella
			Daella Daeoril
			Elaena Leaysa Naerys Rhae Rhaelle Rhaeys Rhaelinor Saenrys
			Syaella
			
			#CANON
			Talisa:1000 Trianna:2000 Selaesori:700
			
					
			#NEW FEMALE NAMES
		
			Dileah Elaera Onalella Nereah Erola Norianna Minanea Inerylla Aleah Tirora Firanissa Phireah Serera Sirona Triannya Taeynea Daenyea Arreyana
			Nesianna Helanea Ahrysa Phirona Philona Faeyella Laresha Velle Nesynea Melyna Ferysa Vellena Harreyana Irylla Faeyolana Eyolana Vaerosha
			Daenala Trianira Neseyana Henolana Lessynea Helyea Meshaleah Phenoreah Henaleah Tiryna Henerah Ahrianna Wenyla Milaya Meshaena Penyla
			Ilola Firaneah Nesoreah Ferylea Mineya Phenolana Leresa Nilyna Phenaria Wynesa Phirella Sera Wenela Doresa Doreza Nolona Onalona Onalya
			Elynea Arraya Harraya Sirysha Ranira Vellya Laensa Dorianna Vellona Dora Elaya Naena Maena Onalyna Seraena Nesora Feria Nesissa Saelysha
			Vaena Haena Qaena Yaena Gaena Caena Zaena Aena Saena Raena Philario Pysaria Nysaria Nesa Melolla Firanya Firania Firanela Arenela Alea
			Bysaria Tysaria Lysaria Dysaria Laenesa Lanesa Enesa Ahraena Faeyila Nilora Melina Nilina Tirysha Melala Sireya Tirola Zarya Zoronna Zargana
			Serylla Taenolana Laenysha Serylla Sirana Siranah Ilona Siraya Melylla Voreya Vorylea Minelna Daenirah Triannolla Saella Mellianna Morylea Moreya
			Wyna Lessella Helissa Nesila Taenya Vorynea Lessera Hena Orala Serola	

			Ilia Nilde Venusta Licya Zaarina Lucrezzia			
			
			Daesanne:50 Bahra:50 Saerla:50 Jelaenya:50 Banera:50 Nesaenys:50 Saenehna:50 Maelerys:50 Daenera:50 Jaelala:50 Alaesys:50 Haehnae:50 Nesaessa:50 Bhaelys:50 Nelaenys:50
			Naelarla:50 Baenna:50 Jelaenys:50 Daenella:50 Bhaenla:50 Haehra:50 Selaela:50 Daemys:50 Aenela:50 Renaerla:50 Bassa:50 Rhaenemera:50 Nesaehnae:50 Saenelys:50 Bara:50 Saehra:50
			Vaehra:50 Jelaehra:50 Aenehnae:50 Salamera:50  Naesella:50 Saechnae:50 Daenerya:50 Relaechnae:50 Baesa:50 Alaesanne:50 Daella:50 Relaera:50 Nesaesanne:50 Vysehnae:50 Vaena:50
			Alynna:50 Haelerla:50 Relaesa:50 Jaemala:50 Daehrys:50 Maesanne:50 Aenesa:50 Malaesa:50 Saena:50 Daehyrs:50 Nelaerya:50 Maesanne:50 Saerla:50 Aenesa:50 Malaesa:50 Saelara:50
			Relaesanne:50 Bhaela:50 Delaehrys:50 Alaenla:50 Rhaenla:50 Rhaera:50 Jaehyrs:50 Saehra:50 Saenessa:50 Alaesanne:50 Jaelanys:50 Elaela:50 Hemys:50 Henya:50 Rhaehra:50 Raenenyra:50
			Haenyra:50 Vaesys:50 Vysella Naenera Haemera Malaenyra Nelaella Haelena Aerena Alaenys Naeressa Hessa Rhaenella Daenea Jaelassa Eraemera Delaenyra Alaenya Vaerla Malaernera:50 Maesys
			Jaerya:50 Alaera:50 Jelaehra:50 Vaenera:50 Elaerys Rhaenna Rhaessa Aenerya Daenehyrs Naenla Bassa Saenys Bhaenyra Malaehra Saelasa Baenys Vhaenyra Saenera Daenna
		}
		
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 15
		mat_grm_name_chance = 10
		mother_name_chance = 0
		
		disinherit_from_blinding = yes
		
		modifier = default_culture_modifier
	}
	
	mantaryan = {
		graphical_cultures = { westvalyriangfx republicsgfx }
		
		color = { 0.45 0.45 0.45 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		character_modifier = {
			dragon_hatching = 1
			dragon_taming = 4
		}
		
		male_names = {
			#CANON
			
			Aenar Aelix
			Daegar Haerys
			Jaekar
			Matarys
			Orys Qoherys
			Vaeron Vaekar Vaermon Jaegon Jaemond Gaelar
			
			Jaenyx:50 Rahaerion:50 Taeron:50 Balaeron:50 Aegaron:50 Balaemor:50 Taegon:50 Tahaelor:50 Matadar:50 Rahaedar:50 Jaelarys:50 Balaemor:50 Laelar:50 Aenyx:50 Malaelar:50 Baemorys:50 	
			Laemond:50 Mamyx:50 Aerygon:50 Baesegarys:50 Gahaenyx:50 Jaemar:50 Raegar:50 Vaemarys:50 Valyx:50 Jayns:50 Laerion:50 Garaelor:50 Jacaelar:50 Daemalon:50 Tyraenyx:50 Daemavar:50 Laegon:50
			Malaenar:50 Galaerys:50 Aenarr:50 Maehamon:50 Matalor:50 Tyraerys:50 Aerymion:50 Balaenyx:50 Raegel:50 Tahaegor:50 Aeravar:50 Visemar:50 Rahaemyx:50 Tahaevon:50 Taedar:50 Maraenar:50
			Maevon:50 Matamor:50 Baesegarys:50 Aedor:50 Jaeremion:50 Aemor:50 Tyraemion:50 Aerydar:50 Raerion:50 Aeranyx:50 Daeranyx:50 Aeragar:50 Matamorys:50 Tahaegaron:50 Matamion:50 Malaemar:50
			Vanor:50 Gaeron:50 Vaegor:50 Aegar:50 Lucaegaron:50 Gaelyx:50 Virys:50 Vimion:50 Virys:50 Aeryrys:50 Visegel:50 Aeganys:50 Balaegor:50 Gahaeron:50 Baegel:50 Vadar:50 Jaemond:50 Jaerevon:50
			Jaemion:50 Tyraevar:50 Gahaemorys:50 Laenarr:50 Ragaemon:50 Vahaenys:50 Aerarion:50 Rhaemar:50 Jaenar:50 Vanys:50 Galaemar:50 Aevor:50 Vagaron:50 Yraenar:50 Baesemor:50 Baseimion:50 Tyraemon:50
			Maelyx:50 Rahaerys:50 Taecedar:50 Tahaemarr:50 Maerion:50 Gaemon:50 Daeragon:50 Taenarr:50 Rhaenar:50 Aemon:50			
		}
		female_names = {
			
			Allyria Alearys
			Cyeana Cymella
			Daella Daeoril
			Elaena Leaysa Naerys Rhae Rhaelle Rhaeys Rhaelinor Saenrys
			Syaella
				
			Daesanne:50 Bahra:50 Saerla:50 Jelaenya:50 Banera:50 Nesaenys:50 Saenehna:50 Maelerys:50 Daenera:50 Jaelala:50 Alaesys:50 Haehnae:50 Nesaessa:50 Bhaelys:50 Nelaenys:50
			Naelarla:50 Baenna:50 Jelaenys:50 Daenella:50 Bhaenla:50 Haehra:50 Selaela:50 Daemys:50 Aenela:50 Renaerla:50 Bassa:50 Rhaenemera:50 Nesaehnae:50 Saenelys:50 Bara:50 Saehra:50
			Vaehra:50 Jelaehra:50 Aenehnae:50 Salamera:50  Naesella:50 Saechnae:50 Daenerya:50 Relaechnae:50 Baesa:50 Alaesanne:50 Daella:50 Relaera:50 Nesaesanne:50 Vysehnae:50 Vaena:50
			Alynna:50 Haelerla:50 Relaesa:50 Jaemala:50 Daehrys:50 Maesanne:50 Aenesa:50 Malaesa:50 Saena:50 Daehyrs:50 Nelaerya:50 Maesanne:50 Saerla:50 Aenesa:50 Malaesa:50 Saelara:50
			Relaesanne:50 Bhaela:50 Delaehrys:50 Alaenla:50 Rhaenla:50 Rhaera:50 Jaehyrs:50 Saehra:50 Saenessa:50 Alaesanne:50 Jaelanys:50 Elaela:50 Hemys:50 Henya:50 Rhaehra:50 Raenenyra:50
			Haenyra:50 Vaesys:50 Vysella Naenera Haemera Malaenyra Nelaella Haelena Aerena Alaenys Naeressa Hessa Rhaenella Daenea Jaelassa Eraemera Delaenyra Alaenya Vaerla Malaernera:50 Maesys
			Jaerya:50 Alaera:50 Jelaehra:50 Vaenera:50 Elaerys Rhaenna Rhaessa Aenerya Daenehyrs Naenla Bassa Saenys Bhaenyra Malaehra Saelasa Baenys Maena		
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		disinherit_from_blinding = yes
		
		modifier = default_culture_modifier
	}
	
	tolosi = {
		graphical_cultures = { westvalyriangfx republicsgfx }
		
		color = { 0.3 0.3 0.3 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		character_modifier = {
			dragon_hatching = 1
			dragon_taming = 4
		}
		
		male_names = {
			#CANON

			Aenar Aelix
			Daegar Haerys
			Jaekar
			Matarys
			Orys Qoherys
			Vaeron Vaekar Vaermon Jaegon Jaemond Gaelar
					
			Jaenyx:50 Rahaerion:50 Taeron:50 Balaeron:50 Aegaron:50 Balaemor:50 Taegon:50 Tahaelor:50 Matadar:50 Rahaedar:50 Jaelarys:50 Balaemor:50 Laelar:50 Aenyx:50 Malaelar:50 Baemorys:50 	
			Laemond:50 Mamyx:50 Aerygon:50 Baesegarys:50 Gahaenyx:50 Jaemar:50 Raegar:50 Vaemarys:50 Valyx:50 Jayns:50 Laerion:50 Garaelor:50 Jacaelar:50 Daemalon:50 Tyraenyx:50 Daemavar:50 Laegon:50
			Malaenar:50 Galaerys:50 Aenarr:50 Maehamon:50 Matalor:50 Tyraerys:50 Aerymion:50 Balaenyx:50 Raegel:50 Tahaegor:50 Aeravar:50 Visemar:50 Rahaemyx:50 Tahaevon:50 Taedar:50 Maraenar:50
			Maevon:50 Matamor:50 Baesegarys:50 Aedor:50 Jaeremion:50 Aemor:50 Tyraemion:50 Aerydar:50 Raerion:50 Aeranyx:50 Daeranyx:50 Aeragar:50 Matamorys:50 Tahaegaron:50 Matamion:50 Malaemar:50
			Vanor:50 Gaeron:50 Vaegor:50 Aegar:50 Lucaegaron:50 Gaelyx:50 Virys:50 Vimion:50 Virys:50 Aeryrys:50 Visegel:50 Aeganys:50 Balaegor:50 Gahaeron:50 Baegel:50 Vadar:50 Jaemond:50 Jaerevon:50
			Jaemion:50 Tyraevar:50 Gahaemorys:50 Laenarr:50 Ragaemon:50 Vahaenys:50 Aerarion:50 Rhaemar:50 Jaenar:50 Vanys:50 Galaemar:50 Aevor:50 Vagaron:50 Yraenar:50 Baesemor:50 Baseimion:50 Tyraemon:50
			Maelyx:50 Rahaerys:50 Taecedar:50 Tahaemarr:50 Maerion:50 Gaemon:50 Daeragon:50 Taenarr:50 Rhaenar:50 Aemon:50
			
		}
		female_names = {
			
			Allyria Alearys
			Cyeana Cymella
			Daella Daeoril
			Elaena Leaysa Naerys Rhae Rhaelle Rhaeys Rhaelinor Saenrys
			Syaella
				
			Daesanne:50 Bahra:50 Saerla:50 Jelaenya:50 Banera:50 Nesaenys:50 Saenehna:50 Maelerys:50 Daenera:50 Jaelala:50 Alaesys:50 Haehnae:50 Nesaessa:50 Bhaelys:50 Nelaenys:50
			Naelarla:50 Baenna:50 Jelaenys:50 Daenella:50 Bhaenla:50 Haehra:50 Selaela:50 Daemys:50 Aenela:50 Renaerla:50 Bassa:50 Rhaenemera:50 Nesaehnae:50 Saenelys:50 Bara:50 Saehra:50
			Vaehra:50 Jelaehra:50 Aenehnae:50 Salamera:50  Naesella:50 Saechnae:50 Daenerya:50 Relaechnae:50 Baesa:50 Alaesanne:50 Daella:50 Relaera:50 Nesaesanne:50 Vysehnae:50 Vaena:50
			Alynna:50 Haelerla:50 Relaesa:50 Jaemala:50 Daehrys:50 Maesanne:50 Aenesa:50 Malaesa:50 Saena:50 Daehyrs:50 Nelaerya:50 Maesanne:50 Saerla:50 Aenesa:50 Malaesa:50 Saelara:50
			Relaesanne:50 Bhaela:50 Delaehrys:50 Alaenla:50 Rhaenla:50 Rhaera:50 Jaehyrs:50 Saehra:50 Saenessa:50 Alaesanne:50 Jaelanys:50 Elaela:50 Hemys:50 Henya:50 Rhaehra:50 Raenenyra:50
			Haenyra:50 Vaesys:50 Vysella Naenera Haemera Malaenyra Nelaella Haelena Aerena Alaenys Naeressa Hessa Rhaenella Daenea Jaelassa Eraemera Delaenyra Alaenya Vaerla Malaernera:50 Maesys
			Jaerya:50 Alaera:50 Jelaehra:50 Vaenera:50 Elaerys Rhaenna Rhaessa Aenerya Daenehyrs Naenla Bassa Saenys Bhaenyra Malaehra Saelasa	Baenys	Maena	
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		disinherit_from_blinding = yes
		
		modifier = default_culture_modifier
	}
	
	elyrian = {
		graphical_cultures = { westvalyriangfx republicsgfx }
		
		color = { 0.55 0.55 0.55 }
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		character_modifier = {
			dragon_hatching = 1
			dragon_taming = 4
		}
		
		male_names = {
			#CANON
			
			Aenar Aelix
			Daegar Haerys
			Jaekar
			Matarys
			Orys Qoherys
			Vaeron Vaekar Vaermon Jaegon Jaemond Gaelar
				
			Jaenyx:50 Rahaerion:50 Taeron:50 Balaeron:50 Aegaron:50 Balaemor:50 Taegon:50 Tahaelor:50 Matadar:50 Rahaedar:50 Jaelarys:50 Balaemor:50 Laelar:50 Aenyx:50 Malaelar:50 Baemorys:50 	
			Laemond:50 Mamyx:50 Aerygon:50 Baesegarys:50 Gahaenyx:50 Jaemar:50 Raegar:50 Vaemarys:50 Valyx:50 Jayns:50 Laerion:50 Garaelor:50 Jacaelar:50 Daemalon:50 Tyraenyx:50 Daemavar:50 Laegon:50
			Malaenar:50 Galaerys:50 Aenarr:50 Maehamon:50 Matalor:50 Tyraerys:50 Aerymion:50 Balaenyx:50 Raegel:50 Tahaegor:50 Aeravar:50 Visemar:50 Rahaemyx:50 Tahaevon:50 Taedar:50 Maraenar:50
			Maevon:50 Matamor:50 Baesegarys:50 Aedor:50 Jaeremion:50 Aemor:50 Tyraemion:50 Aerydar:50 Raerion:50 Aeranyx:50 Daeranyx:50 Aeragar:50 Matamorys:50 Tahaegaron:50 Matamion:50 Malaemar:50
			Vanor:50 Gaeron:50 Vaegor:50 Aegar:50 Lucaegaron:50 Gaelyx:50 Virys:50 Vimion:50 Virys:50 Aeryrys:50 Visegel:50 Aeganys:50 Balaegor:50 Gahaeron:50 Baegel:50 Vadar:50 Jaemond:50 Jaerevon:50
			Jaemion:50 Tyraevar:50 Gahaemorys:50 Laenarr:50 Ragaemon:50 Vahaenys:50 Aerarion:50 Rhaemar:50 Jaenar:50 Vanys:50 Galaemar:50 Aevor:50 Vagaron:50 Yraenar:50 Baesemor:50 Baseimion:50 Tyraemon:50
			Maelyx:50 Rahaerys:50 Taecedar:50 Tahaemarr:50 Maerion:50 Gaemon:50 Daeragon:50 Taenarr:50 Rhaenar:50 Aemon:50		
		}
		female_names = {
			
			Allyria Alearys
			Cyeana Cymella
			Daella Daeoril
			Elaena Leaysa Naerys Rhae Rhaelle Rhaeys Rhaelinor Saenrys
			Syaella
				
			Daesanne:50 Bahra:50 Saerla:50 Jelaenya:50 Banera:50 Nesaenys:50 Saenehna:50 Maelerys:50 Daenera:50 Jaelala:50 Alaesys:50 Haehnae:50 Nesaessa:50 Bhaelys:50 Nelaenys:50
			Naelarla:50 Baenna:50 Jelaenys:50 Daenella:50 Bhaenla:50 Haehra:50 Selaela:50 Daemys:50 Aenela:50 Renaerla:50 Bassa:50 Rhaenemera:50 Nesaehnae:50 Saenelys:50 Bara:50 Saehra:50
			Vaehra:50 Jelaehra:50 Aenehnae:50 Salamera:50  Naesella:50 Saechnae:50 Daenerya:50 Relaechnae:50 Baesa:50 Alaesanne:50 Daella:50 Relaera:50 Nesaesanne:50 Vysehnae:50 Vaena:50
			Alynna:50 Haelerla:50 Relaesa:50 Jaemala:50 Daehrys:50 Maesanne:50 Aenesa:50 Malaesa:50 Saena:50 Daehyrs:50 Nelaerya:50 Maesanne:50 Saerla:50 Aenesa:50 Malaesa:50 Saelara:50
			Relaesanne:50 Bhaela:50 Delaehrys:50 Alaenla:50 Rhaenla:50 Rhaera:50 Jaehyrs:50 Saehra:50 Saenessa:50 Alaesanne:50 Jaelanys:50 Elaela:50 Hemys:50 Henya:50 Rhaehra:50 Raenenyra:50
			Haenyra:50 Vaesys:50 Vysella Naenera Haemera Malaenyra Nelaella Haelena Aerena Alaenys Naeressa Hessa Rhaenella Daenea Jaelassa Eraemera Delaenyra Alaenya Vaerla Malaernera:50 Maesys
			Jaerya:50 Alaera:50 Jelaehra:50 Vaenera:50 Elaerys Rhaenna Rhaessa Aenerya Daenehyrs Naenla Bassa Saenys Bhaenyra Malaehra Saelasa	Baenys	Maena	
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		disinherit_from_blinding = yes
		
		modifier = default_culture_modifier
	}
}

dragon_group = {
	graphical_cultures = { westerngfx } # buildings, portraits, units
	
	dragon_culture = {
		graphical_cultures = { dragongfx }
		
		used_for_random = no
		allow_in_ruler_designer = no
		alternate_start = { 
			OR = {
				has_alternate_start_parameter = { key = special_culture value = animal_kingdoms } 
				has_alternate_start_parameter = { key = special_culture value = animal_world }
				has_alternate_start_parameter = { key = special_culture value = all } 
			}	
		}
		
		color = { 0.15 0.15 0.15 }
		
		male_names = {
			#Canon
			Quicksilver Seasmoke Urrax "Grey Ghost" 
			Stormcloud Morghul Shrykos Terrax
			#Male only
			Arrax Balerion Caraxes Vermithor Sunfyre Tyraxes Vermax Viserion Rhaegal
			
			#Non-Canon
			Azuron Aderex Azantyr Azantys Acranion Artaxes Aderion Arhugon Angogon Anogaron Archon Aquos Adereon Aeksion Atsiox Arryn
			Anogaros Anaxigon Aenion Aegal Aeriax Aetrox Ancylagon Archonei Aestion Ancalion Alatoraxes Bantis Braedazma Blackclaw
			Blackhorn Blazoron Belmurtys Braedax Braedion Banzolkal Blackfyre Baelal Baeliphax Bamuxes Blackwing Bloodfyre		
			Chalfyre Chroyax
			Drythron Daenal Darkscale Drakaryx Deltherion Darkfyre Draggox Duskfyre Darkwing Daomio Demagon Dynax
			Egros Embarelion Eberron Errkos Essoval Essarix Elyrion Ebrion Elenixes Erinnon
			Firzon Fusrodax Firestorm Fyrax Firkrax Fafnar Falkael Forgesong Fasharyon Fangrys Famlyre  
			Fornaxes Firecatcher
			Galeglider Gorthalon Goldfyre Goldscale Ghiscar Glimmer Gorgorkos Greyfyre Grymax Greencomet Gloombringer Gravefyre Gorgosson Galry Gelenka Gevives Gurogon Gemtail
			Haryz Hegagon Helfyre Heartfyre Henkirix Highagon Hurax
			Issilgor Ilthax Illithinos Ignerion Idanax Ioragon Irudyx 
			Jaharix Johagon Jelmazma Jedrien Jadewing Jaedos Jaesax Jaqiarzir Jehikagon Jehikarys Jentyx
			Knightslayer Krythax Keligon Kastakonir Kaerinax Kastax Keliothor Konoron Korzion Kostobion
			Loramon Limagon Likaxes Lovghar Lifefyre Laenion Laengar Lorcan Lys Laehurlion Laraxes Lilagon Lykax
			Mountdweller Moonshadow Monerys Morghon Malrax Maelar Maelstral Malys Moonfyre Myr Mantaryon Mhysa Melexes Merbugon Midnight Majesty
			Nedys Nightfyre Neltharion Nightpyre Naqes Nedenkon Nektogon Nightwind
			Ossenaxes Odikagon Osygon Orzadrize Oktinuqir Orion Oros Obaraxes Ondorion Onos Orbarion Ozzalagon
			Pryagon Perzypeldax Pythax Porungar Pestyre Provorpion Predatorion Pentos Parklon Peldion Perzys Prumiax Pryjagon
			Qilonarion Qelovaedar Qanax Qelos Qridron 
			Razys Rehitys Raqagon Rhaenal Rhaeal Rhaenar Ryax Rhaetherion Raselion Rhoynar Rhyos Raqiron Rhovagon Rizaxes Rovax Rune Rainfyre
			Solthys Starhunter Starfyre Spyrax Soulblaze Scales Slythe Serporoth Skysailor Starwing Seawing Shraynor Skysong Steelwing Sphinx Selhoron Sarnor Sambaron Senagon Sonax Sovegon Suvion Sylviax Solstice Snow Sapphire
			Tormoxen Thaxilix Telfyre Tallfyre Taeral Taegar Taerion Tiamaxes Toothless Telarion Trogdax Timerion Tyrosh Tyriax Tolosion Tikun Timpax Turgon Tyvaron
			Valeryx Vhaegal Vassarion Vezolkys Vezojaes Vaegelion Vaedar Visenion Vaelax Volgavys Vishyap Valryon Viserax Valyrion Volon Volantix Valysarion Velos Vesteros Vaedagon Vejes
			Whiteclaw "Winged Stranger" Whitewing
			Yndros
			Zuvdar Zobrinogar Zoksax Ziragon Zalagys Zaldrizarys Zobrigon Zobrughul Zaenor Zaeral Zalagon Zaldrizes Zoklax Zugerion 
			
			#Male only
			Aegax Baelax Aemax Althorio Bakkalon
			Darys
			Bullbreaker
			Darondarys Daerax Daemax Deathwing Doom
			Fenrion Fellfang
			Glaurax
			Kepaxes
			Matarion Luciferon Maegal Maekal
			Ondordarys Peasantslayer
			Rhaegax
			Smaugon Stormwyrm Saagael
			Tresyx
			Verathion Vermithrax Valonqar Valzyryx Vandis Valax Velkrys
			Wolfbane Wraithwing		 
		}
		female_names = {
			#Canon
			Quicksilver Seasmoke Urrax "Grey Ghost" 
			Stormcloud Morghul Shrykos Terrax
			#Female only			
			Dreamfyre Meleys Meraxes Moondancer Morning Silverwing Syrax Tessarion Vhagar
			
			#Non-Canon
			Azuron Aderex Azantyr Azantys Acranion Artaxes Aderion Arhugon Angogon Anogaron Archon Aquos Adereon Aeksion Atsiox Arryn
			Anogaros Anaxigon Aenion Aegal Aeriax Aetrox Ancylagon Archonei Aestion Ancalion Alatoraxes Bantis Braedazma Blackclaw
			Blackhorn Blazoron Belmurtys Braedax Braedion Banzolkal Blackfyre Baelal Baeliphax Bamuxes Blackwing Bloodfyre		
			Chalfyre Chroyax
			Drythron Daenal Darkscale Drakaryx Deltherion Darkfyre Draggox Duskfyre Darkwing Daomio Demagon Dynax
			Egros Embarelion Eberron Errkos Essoval Essarix Elyrion Ebrion Elenixes Erinnon
			Firzon Fusrodax Firestorm Fyrax Firkrax Fafnar Falkael Forgesong Fasharyon Fangrys Famlyre  
			Fornaxes Firecatcher
			Galeglider Gorthalon Goldfyre Goldscale Ghiscar Glimmer Gorgorkos Greyfyre Grymax Greencomet Gloombringer Gravefyre Gorgosson Galry Gelenka Gevives Gurogon Gemtail
			Haryz Hegagon Helfyre Heartfyre Henkirix Highagon Hurax
			Issilgor Ilthax Illithinos Ignerion Idanax Ioragon Irudyx 
			Jaharix Johagon Jelmazma Jedrien Jadewing Jaedos Jaesax Jaqiarzir Jehikagon Jehikarys Jentyx
			Knightslayer Krythax Keligon Kastakonir Kaerinax Kastax Keliothor Konoron Korzion Kostobion
			Loramon Limagon Likaxes Lovghar Lifefyre Laenion Laengar Lorcan Lys Laehurlion Laraxes Lilagon Lykax
			Mountdweller Moonshadow Monerys Morghon Malrax Maelar Maelstral Malys Moonfyre Myr Mantaryon Mhysa Melexes Merbugon Midnight Majesty
			Nedys Nightfyre Neltharion Nightpyre Naqes Nedenkon Nektogon Nightwind
			Ossenaxes Odikagon Osygon Orzadrize Oktinuqir Orion Oros Obaraxes Ondorion Onos Orbarion Ozzalagon
			Pryagon Perzypeldax Pythax Porungar Pestyre Provorpion Predatorion Pentos Parklon Peldion Perzys Prumiax Pryjagon
			Qilonarion Qelovaedar Qanax Qelos Qridron 
			Razys Rehitys Raqagon Rhaenal Rhaeal Rhaenar Ryax Rhaetherion Raselion Rhoynar Rhyos Raqiron Rhovagon Rizaxes Rovax Rune Rainfyre
			Solthys Starhunter Starfyre Spyrax Soulblaze Scales Slythe Serporoth Skysailor Starwing Seawing Shraynor Skysong Steelwing Sphinx Selhoron Sarnor Sambaron Senagon Sonax Sovegon Suvion Sylviax Solstice Snow Sapphire
			Tormoxen Thaxilix Telfyre Tallfyre Taeral Taegar Taerion Tiamaxes Toothless Telarion Trogdax Timerion Tyrosh Tyriax Tolosion Tikun Timpax Turgon Tyvaron
			Valeryx Vhaegal Vassarion Vezolkys Vezojaes Vaegelion Vaedar Visenion Vaelax Volgavys Vishyap Valryon Viserax Valyrion Volon Volantix Valysarion Velos Vesteros Vaedagon Vejes
			Whiteclaw "Winged Stranger" Whitewing
			Yndros
			Zuvdar Zobrinogar Zoksax Ziragon Zalagys Zaldrizarys Zobrigon Zobrughul Zaenor Zaeral Zalagon Zaldrizes Zoklax Zugerion
			
			#Female only		
			Bantisdaria
			Darilaros Daenaryon
			Elaenal
			Geliondaria Grace
			Haedar Jaeherion Jaehio
			Litse
			Naerion Moonblaze Mandiax Munax Nightdancer Naejos Pantera
			Rhaellax Rhaenion Rhaenyral Raqnon Rinax Ruklon Rainbow	
			Saphirax Sunshine Skydancer Sundancer Sparkle
			Talaxes
			Zaldrizaria		  
		}
			
		from_dynasty_prefix = "of "
		#founder_named_dynasties = yes
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 0
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 0
		mat_grm_name_chance = 0
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}