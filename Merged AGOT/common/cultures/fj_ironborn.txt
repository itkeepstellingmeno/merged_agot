iron_isles_culture = {
	graphical_cultures = { westerngfx } # portraits

	old_ironborn = {
		graphical_cultures = { ironborngfx } # portraits

		color = { 0.1 0.1 0.1 }

		alternate_start = {
			year < 7100 
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			Adrack Aeron Aggar Aladale Alester Alvyn Alyn Ambrode Andrik Alton Arthur
			Baelor Balon Bennarion Beron Boremund Burton
			Cadwyl Cotter Cragorn Cromm Craghorn
			Daegon Dagmer Dagon Dale Denys Donel Donnor Drennan Dunstan Dykk Dalton
			Eerl Eldiss Eldred Emmond Endehar Erik Euron Erich
			Fralegg Fergon
			Gelmarr Germund Gevin Gormond Gorold Godric Gran Greydon Gylbert Gyles Gynir Goren Gunthor Grimm
			Hagen Harl Harlon Harmund Harrag Harras Harren Harwyn Hilmar Hotho Harrock Horgan Hagon Harlan Harron Harrald Hake
			Jon Joseran Joron
			Kemmett Kenned Kromm
			Lenwood Lorren Lucas Lucimore Loron Lodos
			Manfryd Maron Meldred
			Norjen Norne Nute
			Othgar
			Pate
			Qalen Qarl Qhored Quellon Quenton Qhorwyn Qhorin
			Ragnor Ralf Robin Rodrik Roggon Rolfe Romny Rook Roryn Rus Rymolf Rognar Ravos Regnar
			Sargon Sawane Sigfry Sigfryd Sigrin Skyte Steffar Steffarion Stygg Sylas Symond
			Tarle Theomore Theon Thormor Todric Tom Toron Torgon Torwold Tristifer Triston Tymor Torwyn
			Ulf Uller Urek Urragon Urras Urrigon Urron Urzen Urrathon
			Vickon Victarion Veron
			Waldon Werlag Wex Will Wulfe Wulfgar
			Ygon Yohn
			
			#DAMOC
			Erne Agil Ormm Ottar Torbund Odd Dan Toke Rance Ragnal Balder Grimm Hakon Gunnar Gorm Mats Hrolf Faste Falki Konrad
		}
		female_names = {
			Alannys Asha:1000 Arwyn
			Barbrey Beony Brella
			Dorna
			Esgred:500
			Falia Falyse Ferny Frenya Frynne
			Gretchel Grisel Grisella Gwin Gwynesse Gwyneth Gysella
			Harma Harra Helya
			Jeyne Jonella Jonnela Jorelle
			Malora Margot Megga Meredyth Mhaegan Mhaegen Moelle Morna Munda Myrielle
			Ravella Rhonda Roelle Ryella
			Selyse Shierle
			Talla
			Unella
			Victaria:500
			Wynafrei Wynafryd
			Yara:500 Ynys Ysilla
			Zia
			
			#PLACEHOLDER IRONBORN FEMALES
			Agmuna Asgrima Aslaka Arya Aly Arsa Arana Asharra Ana Asa Abni Aynika Ayssi Agnis
			Beinerra Bhurta Barrya Barsa Barana
			Ernmunda Errika Eddara Ersa Erana Errya Efla Ela
			Cromma Cholvi Corah Ceyri
			Farra
			Dags
			Gritha Gerika Ghaela Gremma Ghauda Gella Ghulmi
			Hegga Herrika Helgarda Hagnaeh Harra
			Irica 
			Jonna Jomara Jolfa Johsi Jhena
			Kella
			Larya Larsa Larana Laransa Lyssa Lil
			Margadra Millga Mabs
			Narrya Narrsa Narrana Noshi Nanna
			Orrsa Orrana Orya Oshara Ora Olma
			Ransa Raly Raddera Ryka Ryloka Rokah Riga
			Syga Sygara Syganna Sygarda turri Skeyga Snela 
			Skati Skoni Syri Suna Soyni Syansa
			Toraka Tyosa Tekkeda Tarya Tansa Thalvi Tralmi Trabi Thoki Thesa
			Ulfara Utmara
			Valy Varana
			Yasha
			Zosha Zansa
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		allow_looting = yes
		seafarer = yes

		modifier = backward_culture_modifier
	}

	ironborn = {
		graphical_cultures = { ironborngfx } # portraits

		color = { 0.25 0.25 0.25 }

		alternate_start = {
			year >= 7100 
			NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } }
		}

		male_names = {
			Adrack Aeron Aggar Aladale Alester Alvyn Alyn Ambrode Andrik Alton Arthur
			Baelor Balon Bennarion Beron Boremund Burton
			Cadwyl Cotter Cragorn Cromm Craghorn
			Daegon Dagmer Dagon Dale Denys Donel Donnor Drennan Dunstan Dykk Dalton
			Eerl Eldiss Eldred Emmond Endehar Erik Euron Erich
			Fralegg Fergon
			Gelmarr Germund Gevin Gormond Gorold Godric Gran Greydon Gylbert Gyles Gynir Goren Gunthor Grimm
			Hagen Harl Harlon Harmund Harrag Harras Harren Harwyn Hilmar Hotho Harrock Horgan Hagon Harlan Harron Harrald Hake
			Jon Joseran Joron
			Kemmett Kenned Kromm
			Lenwood Lorren Lucas Lucimore Loron Lodos
			Manfryd Maron Meldred
			Norjen Norne Nute
			Othgar
			Pate
			Qalen Qarl Qhored Quellon Quenton Qhorwyn Qhorin
			Ragnor Ralf Robin Rodrik Roggon Rolfe Romny Rook Roryn Rus Rymolf Rognar Ravos Regnar
			Sargon Sawane Sigfry Sigfryd Sigrin Skyte Steffar Steffarion Stygg Sylas Symond
			Tarle Theomore Theon Thormor Todric Tom Toron Torgon Torwold Tristifer Triston Tymor Torwyn
			Ulf Uller Urek Urragon Urras Urrigon Urron Urzen Urrathon
			Vickon Victarion Veron
			Waldon Werlag Wex Will Wulfe Wulfgar
			Ygon Yohn
			
			#DAMOC
			Erne Agil Ormm Ottar Torbund Odd Dan Toke Rance Ragnal Balder Grimm Hakon Gunnar Gorm Mats Hrolf Faste Falki Konrad
		}
		female_names = {
			Alannys Asha:1000 Arwyn
			Barbrey Beony Brella
			Dorna
			Esgred:500
			Falia Falyse Ferny Frenya Frynne
			Gretchel Grisel Grisella Gwin Gwynesse Gwyneth Gysella
			Harma Harra Helya
			Jeyne Jonella Jonnela Jorelle
			Malora Margot Megga Meredyth Mhaegan Mhaegen Moelle Morna Munda Myrielle
			Ravella Rhonda Roelle Ryella
			Selyse Shierle
			Talla
			Unella
			Victaria:500
			Wynafrei Wynafryd
			Yara:500 Ynys Ysilla
			Zia
			
			#PLACEHOLDER IRONBORN FEMALES
			Agmuna Asgrima Aslaka Arya Aly Arsa Arana Asharra Ana Asa Abni Aynika Ayssi Agnis
			Beinerra Bhurta Barrya Barsa Barana
			Ernmunda Errika Eddara Ersa Erana Errya Efla Ela
			Cromma Cholvi Corah Ceyri
			Farra
			Dags
			Gritha Gerika Ghaela Gremma Ghauda Gella Ghulmi
			Hegga Herrika Helgarda Hagnaeh Harra
			Irica 
			Jonna Jomara Jolfa Johsi Jhena
			Kella
			Larya Larsa Larana Laransa Lyssa Lil
			Margadra Millga Mabs
			Narrya Narrsa Narrana Noshi Nanna
			Orrsa Orrana Orya Oshara Ora Olma
			Ransa Raly Raddera Ryka Ryloka Rokah Riga
			Syga Sygara Syganna Sygarda Turri Skeyga Snela 
			Skati Skoni Syri Suna Soyni Syansa
			Toraka Tyosa Tekkeda Tarya Tansa Thalvi Tralmi Trabi Thoki Thesa
			Ulfara Utmara
			Valy Varana
			Yasha
			Zosha Zansa
		}

		from_dynasty_prefix = "of "

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 25
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 5
		mat_grm_name_chance = 20
		mother_name_chance = 0

		allow_looting = yes
		seafarer = yes

		modifier = default_culture_modifier
	}
}