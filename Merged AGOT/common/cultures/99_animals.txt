# Do not change tags in here without changing every other reference to them.
# If adding new groups or cultures, make sure they are unique.

horse_group = {
	graphical_cultures = { horsegfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
			has_alternate_start_parameter = { key = special_culture value = all } 
		}
	}

	horse = {
		graphical_cultures = { horsegfx }
		color = { 0.3 0.1 0.1 }

		male_names = {
			# AGOT horses
			Blood
			Chestnut
			Driftwood
			Glory
			Honor
			Midnight
			Rain
			Smiler
			Stranger
			Thunder
			Wrath

			# Vanilla names
			Arod Asfaloth
			Barnaby Bayard B�ckah�st Bill Blackjack Bojack Bolt Boxer Brag Brego Brunte Bucephalus Bumpkin
			Chetak Cincinnati "Clever Hans" Comanche Copiad
			Dale Dasher Domino
			Fatty Ferd Firefoot Flame Furia
			Glitterhoof "Glue-Boy" Golden Guido Grolle
			Hasufel Helhest Hippo Hornline Horse Houyhnhnm Hunter Hwin
			Incitatus
			J�rvs�faks Jim
			Lightfoot "Lilla Gubben" Lukas Lumpkin
			Marengo Martinique Merrylegs Monty
			Nahar Napoleon N�cken Neck Newton Nightingale
			Oakley
			Palomo Pylon
			Rainbow Red Rio Roheryn Rusher Rusty
			"Sharp-Ears" Sleipnir Snowmane Streiff Sture Stybba Sullivan Swishtail
			Terry Traveller Trigger Trojan Tyke
			Vanderbilt Veillantif Verner Victory
			Windfola "Wise-Nose"
			Yndros
		}
		female_names = {
			# AGOT horses
			Craven
			Dancer
			Flame
			Sweetfoot

			# Vanilla names
			Arod Asfaloth
			B�ckah�st Bonney Brego Bumpkin Buttermilk
			Charlotte Chollima
			Epona
			Fatty Firefoot
			Glitterhoof Gullfaxi
			Hasufel Heidi Helhest Hippo Horse
			Kara Kasztanka
			"Lady Wonder" Lightfoot Lumpkin
			Maggie Magic Mona
			Nahar Napoleon Nightingale
			Paris Pearl Pumpkin Pylon
			Rainbow Red Roheryn Rusher
			Sallie Scout "Sergeant Reckless" "Sharp-Ears" Silhouette Snowmane Spice Stybba Swishtail
			Tingeling Trojan
			Victory
			Windfola "Wise-Nose"
			Yndros
			Zarona
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			clan_sentiment = 10 # What would a nomad horde be without their horses?
			dothraki_opinion = 20 # Mongols love their horses!
		}

		nomadic_in_alt_start = yes
	}
}

cat_group = {
	graphical_cultures = { catgfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
			has_alternate_start_parameter = { key = special_culture value = all }
		}
	}

	cat = {
		graphical_cultures = { catgfx }
		color = { 0.1 0.3 0.1 }

		male_names = {
			Yndros
			Mittens Fluffy Tom Smudge Shadow Zimm Samm Misstoffelees Deuteronomy Pouncival Quaxo Mungojerrie Mozart Simba Tiger Garfield Socrates Salem Toulouse Berlioz Thomas Birk
			"Ser Pounce" Boots King Lord Hyrkoon Prince
			"Basileus Harlocke Spoops" Bazcat Furball Angry Snuggles Lucky Buttons Snowbell Cuddles Crookshanks "Mr. Bigglesworth" Spot Whiskers Lucky Juni Maow Cat Coins Panther
			Frumpkin Chairman Meow Floof Ninja Orion Felix Tim Rowl Lister Crockett Zebedeus Syd Bear Ripper Maurice Jake Sylvester Big Talking Tom Atlas Serafettin Simon Santa Paws
			M'aiq Vincent Hades F�a Tyki Lavi Pan Tjockisen Lotus Morris Shadow Oscar Eskil Shadow Tommiecat Bosse Lasse Basil Zylyn Snooka Maat
			Lord-Admiral-Edward-Hawke-First-Baron-Hawke-First-Lord-of-the-Admiralty Nimitz Katz
		}
		female_names = {
			Pantera
			Yndros
			Missy Kitty Mittens Ginger Princess Bella Sigma Mysan Missan Victoria Rumpelteazer Grizabella Demeter Bombalurina Sooty Suzie Syllabub Luna Midnight Nala Tussan Selma Salem Duchess Marie Signe
			"Lady Whiskers" Queen Lady Nymeria 
			Duchess Marie Signe Paws "Basileus Harlocke Spoops" Bazcat Furball Angry Snuggles Lucky Buttons Snowbell Cuddles Crookshanks "Mr. Bigglesworth" Spot Whiskers Lucky Juni Maow Cat
			Coins Panther Frumpkin Chairman Meow Floof Poppy Yuki Mimmy Stina Witch Caline Doortje Cwtch Tina River Cleo Willow Chloe Bastet Mafdet Aeris Tonks Lisa Myran Lillkissen Daisy
			Sekhmet Nova Birdie Mynta Molly Tjoppan Nyanners Suki Isis Masika Caliope Sor-Juana-In�s-de-la-Cruz Mitzi Mischief Smilla Aggi
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			murder_plot_power_modifier = 0.2 # A lack of morals makes for an efficient killer
			vassal_opinion = -5 # But it doesn't endear them to their subjects
			fertility = 0.25 # Big litters
		}
	}
}

bear_group = {
	graphical_cultures = { beargfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
			has_alternate_start_parameter = { key = special_culture value = all } 
		}
	}

	bear = {
		graphical_cultures = { beargfx }
		color = { 0.1 0.1 0.3 }

		male_names = {
			Asabj�rn
			Baloo Bamse Barnie Bearclaw Bearnabus Bearnard Bernard Bjarne Bj�rn Bj�rne Bj�rn Blizzard Bobo Boo-Boo B�rje
			Cuddles
			Disco
			Esbj�rn
			Fluffy
			Grizzly Grrgrrr Grrrowr Gurr
			Isbj�rn
			John
			Koda Kumamon
			Misha
			Nalle Nanuk Notabear
			Otso
			Rawr Rupert
			Secret "Sir Bearington" Smokey Styrbj�rn Sugar
			Torbj�rn Trinket
			Ursus Uszatek
			Wojtek
			Yogi
		}
		female_names = {
			Bamse Bearnadette Bearnardine Bernadette Bernardine Bobo Brumma Brummelisa
			Cindy Cuddles
			Disco
			Fluffy
			Goldi Grizzly Grrgrrr Grrrowr
			Kumamon
			"Lady Bearington"
			Maja Misha
			Nadja Nalle Nalle-Maja
			Otso
			Rawr
			Secret Snowball Sugar
			Ursine Urszula
			Yogi
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			land_morale = 0.15 # When you're a bear, you have nothing to fear
			combat_rating = 10 # Obvious
		}
	}
}

hedgehog_group = {
	graphical_cultures = { hedgehoggfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
			has_alternate_start_parameter = { key = special_culture value = all }
		}
	}

	hedgehog_culture = {
		graphical_cultures = { hedgehoggfx }
		color = { 0.2 0.3 1.0 }

		male_names = {
			August Aurion
			Blaine
			Crisp Cyrus
			Echinus Erinus
			Galahad Groogy Grumpy
			Hedgehog Hedgepig Hughues
			Kidnae Knucklys
			Lancelot
			Mesyo Micro "Mr. Tiggy-Winkle"
			Nails
			Pincushion Pinecone Pokey Punk
			Quillem Quilliam Quillington
			Ringo Rix
			Sam Sanic Saul Sclaterion Silver Spike Stig
			Thorn
			Wiggles
			Xerxes
		}
		female_names = {
			Algira Amy
			Briar
			Collie Crisp
			Daura
			Ekidney Erina
			Flower
			Ginger Grumpy
			Hedgehog Hemy Hogatha
			Judit
			Leerix
			Meg Mena
			Nimue Nix
			Oona
			Para Pebbles Peggy Pincushion Pinecone Pokey Prickquille
			Rose
			Sam Scarlet Shaquilla Spike Splinthia Sunshine
			Tegan Tisha
			Ursula
			Vendela Ventria
			Wiggles
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			defensive_plot_power_modifier = 0.2 # Hard to get past the quills
			fertility = 0.25 # Big litters
		}

		allow_looting = yes
		seafarer = yes
	}
}

duck_group = {
	graphical_cultures = { duckgfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
			has_alternate_start_parameter = { key = special_culture value = all }
		}
	}

	duck_culture = {
		graphical_cultures = { duckgfx }
		color = { 1.0 1.0 1.0 }

		male_names = {
			Anaheim Arne
			Beaky Blunder Bolivar
			Cake Cornelius
			Daffy Darkwing Dewey Dominic Donald Drake Duck Duckus Dylan
			Eggbert Emelio
			Face
			Gladstone Grape Greg
			Hans Havbard Howard Huck Huey
			Kalle Knase
			Louie Ludwing
			Maarten Mallard M�rten
			Nottington
			Paul Pippi
			Quackel Quacker Quackers Quackmire
			Scrooge
			Waddles Whitewing Winghelm
		}
		female_names = {
			Ammonia Anaheim Anki Aves
			Beaktrice Bentina Blunder
			Cake Chickadee
			Daisy Donna Dora Doris Duck Dumbella
			Face Fanny Feathers
			Gandra Gittan Goldie Gosalyn Grape Gullan G�salin
			Hortensia
			Irynn
			Jemima
			Kajsa Kicki
			Lola Lucinda
			Matilda Molly
			Neilly
			Oona
			Petunia Pippi
			Quackers
			Selma
			Titti Tyra
			Umpa
			Webbygail Winghelmina
			Zeena
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			siege_speed = 0.2 # They just fly over the walls
			fertility = 0.25 # Lots of eggs
		}

		allow_looting = yes
		seafarer = yes
	}
}

dog_group = {
	graphical_cultures = { doggfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
			has_alternate_start_parameter = { key = special_culture value = all }
		}
	}

	dog_culture = {
		graphical_cultures = { doggfx }
		color = { 0.4 0.3 0.0 }

		male_names = {
			Ajax Albert Alfonso Alistair Alwin Amundsen
			Babe Bad Basil Benjamin Blake Bloomer Blossom Bodkin Bonzo Bosun Brantley Bubbler Bubbles Buckler Buddy Bustle
			Caruso Casey Chirgwin Clifford Cotte Counsellor Cujo
			Dali Dash Dog Dogge Doggo Doglas Droopy
			Ed Elmer Eyebright
			Fido Force Forester
			Gazer Gilbert Gnome Good Goofy Greywind Grif Growler
			Hackenschmidt Hebe Henry Hercules Hilary Homer Hurry
			Inky
			Jolity
			Kelvin Kendrik Killer
			Laddie Louis
			Max Maxx Milton Mini "Mr. Wuffles" Much
			Pele-mele Pluck Pluto Psyche
			Rex Riot Rockdove Rome Rozz Ruff
			Sam Sammy Samson Santi Sapphire Saurr Scooby Shadow Shaggydog Skipper Sky Slobbers Spigot Splitlip Spoiler Spot Spotington Spotty Steamer Strongboy Stubborn Sub Summer Sunbeam Swanker
			Timmi Toby Tracks Trigger Trooper
			Ulysses
			Watson Wesley Wistful Wuffler
			Yelp
			Zico Zorro
			
			#Male
			Arrow
			Bolt Bones Buck
			Chase
			Danger
			Fang Fury
			Grunt
			Rage Ranger Rogue
			Scar Shepherd Splinter Storm Striker
			
			#Unisex
			Blaze Breeze
			Charm Cloud Comet Curse
			Dash Dawn Destiny Dust
			Echo
			Faithful
			Ghost Grey
			Hunter
			Mercy Midnight Mist Moon
			Oak Obsidian Omen
			Rain Raven
			Shade Sky Spring Sun
			Tricky
			Wind Winter
		}
		female_names = {
			Alice Annie
			Babe Bad Bella Berta Bessie Bitta Blazer Blondi Bloomer Blossom Bodkin Brigade Bubbler Bubbles Buckler Bustle Butcher
			Carmen Counsellor Craftsman
			Danny Dash Dog Doggo Dolly Domino
			Eleanor Eve Eyebright
			Fencer Fergie Flecka Force Forester Fury
			Gala Gazer Gnome Golda Good Growler Guinevere
			Hebe Hilary Hurry
			Indira Inky
			Jemma Jolity
			Keeper Killer Kisha
			Laika Lance Lassie Lily Lottie Lurcher Lyra
			Maus Menchi Mia Minerva Much
			Naevia Nikki Nova
			Octavia Odine
			Pele-mele Pluck Prowess Psyche
			Riot Rockdove Rome Ronja
			Sadie Sally Shadow Sky Songster Spigot Spoiler Spot Stubborn Sue Sunbeam Swix
			Tavi Tira Tracks Trooper Twiggy
			Watch Wistful Wuffler
			Xohla
			Yelp Yippie
			Zoe

			#Female
			Amber
			Beauty Bitch
			Coral Crystal
			Fern
			Grace
			Indigo Ivy
			Jewel
			Lady
			Melody
			Nymeria
			Princess
			Queen
			Sapphire
			Vapor Velvet Violet
			Willow
			
			#Unisex
			Blaze Breeze
			Charm Cloud Comet Curse
			Dash Dawn Destiny Dust
			Echo
			Faithful
			Ghost Grey
			Hunter
			Mercy Midnight Mist Moon
			Oak Obsidian Omen
			Rain Raven
			Shade Shadow Sky Spring Summer Sun
			Tricky
			Wind Winter
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			general_opinion = 5 # They are good boys
			fertility = 0.25 # Big litters
		}
	}

	direwolf_culture = {
		graphical_cultures = { direwolfgfx }
		color = { 0.1 0.3 0.1 }

		male_names = {
			#Male
			Arrow
			Bolt Bones Buck
			Chase
			Danger
			Fang Fury
			"Grey Wind" Grunt
			Rage Ranger Rogue
			Scar Shepherd Splinter Storm Striker

			#Unisex
			Blaze Breeze
			Charm Cloud Comet Curse
			Dash Dawn Destiny Dust
			Echo
			Frost
			Ghost Grey
			Hunter
			Ice
			Mercy Midnight Mist Moon
			Oak Obsidian Omen
			Rain Raven
			Shade Shadow Sky Snow Spring Summer Sun
			Wind Winter
		}
		female_names = {
			#Female
			Amber
			Beauty
			Coral Crystal
			Fern
			Grace
			Indigo Ivy
			Jewel
			Lady
			Melody
			Nymeria
			Princess
			Queen
			Sapphire
			Vapor Velvet Violet
			Willow

			#Unisex
			Blaze Breeze
			Charm Cloud Comet Curse
			Dash Dawn Destiny Dust
			Echo
			Frost
			Ghost Grey
			Hunter
			Ice
			Mercy Midnight Mist Moon
			Oak Obsidian Omen
			Rain Raven
			Shade Shadow Sky Snow Spring Summer Sun
			Wind Winter
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			fertility = 0.25 # Big litters
		}
	}
}

elephant_group = {
	graphical_cultures = { elephantgfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = all }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
		}
	}

	elephant_culture = {
		graphical_cultures = { elephantgfx }
		color = { 0.8 0.8 0.8 }

		male_names = {
			Abbas Abul Airawat
			Baahi Baal Babur Bakhsh Barbar Batyr Behru Bhaanu Bhadra Booper Brant
			Castor Chotu Cornelius
			Dhriti Dombo Doot
			Elephant
			Gaj Gajendra Ghaazi
			Hannibal Hanno Hasdrubal Hastin
			Jumbo
			Kamboj Kandula
			"Mr. Tusk"
			Pollux Pompadour Pyyrhus
			Setu Snoot Surus
			Tej Toti Trumpo
			Yathra
			Zaeem Zain Zaki
		}
		female_names = {
			Aashi
			Barbar Bhaanu Bhadra Booper
			Celestial
			Daksha Dhriti Dhruti Dombo Doot
			Elephant
			Fria
			Hansken Hruti
			Jaina Jumbo
			Kandula
			Miraan Moksha
			Okki
			Pompadour Pyyrhus
			Ruby
			Setu Sia Snoot
			Trumpo
			Xara
			Yasha
			Zahara Zain Zia Zyva
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier
	}
}

panda_group = {
	graphical_cultures = { redpandagfx }
	alternate_start = {
		OR = {
			has_alternate_start_parameter = { key = special_culture value = animal_kingdoms }
			has_alternate_start_parameter = { key = special_culture value = animal_world }
			has_alternate_start_parameter = { key = special_culture value = all }
		}
	}

	red_panda = {
		graphical_cultures = { redpandagfx }
		color = { 1.0 0.3 0.0 }

		male_names = {
			Albert Ambrus Avi
			Babu Bamboo Barrie Basil Benjamin Berry Bo
			Carson Ceba Chai Chonk
			Eli
			Feng
			Harold
			Ila
			Jackie
			Kaala Katar Kevyn Khusi
			Lhotse
			Manasa Masala Mohini
			Ning Nutmeg
			Oolong
			Pabu Panda Pandolf Pandulf Peyton Phoenix Ponga Puffball Pumori
			Red
			Sabal Sabi Sawyer Semper Shifu Song Spark Stellar Sundar
			Vira
			Wasabi
			Zeya
		}
		female_names = {
			Aurora Avi
			Bamboo Beatrix Berry Betsy
			Ceba Chai Chonk Chota Cini
			Eriel
			Hazel
			Ila
			Jaya Josefin
			Kaala Katar Khusi
			Leafa Liling
			Mali Mambo Manasa Masala Maude Miko
			Nava Nutmeg
			Oolong
			Pabu Panda Pandolfina Panna Phoenix Piya Ponga Puffball
			Ruth
			Sabal Sabi Satya Scarlett Semper Shifu Simone Song Sonika Sonya Sundar Susana
			Vira
			Wasabi
			Yukiko
			Zeya
		}

		prefix = no # The patronym is added as a suffix

		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 50
		mat_grf_name_chance = 0
		father_name_chance = 0

		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 50
		mother_name_chance = 0

		used_for_random = no
		allow_in_ruler_designer = no

		modifier = default_culture_modifier

		character_modifier = {
			vassal_opinion = 5
			land_morale = 0.05 # Territorial
			fertility = -0.15 # Small litters
		}
	}
}