decisions = {
	
	convert_to_storm_god_religion = {
		ai = no
		
		potential = {
			FROM = {
				has_landed_title = c_storms_end
				any_owned_bloodline = {
					has_bloodline_flag = durrandon
				}
				OR = {
					trait = cynical
					trait = arbitrary
					trait = lunatic
					trait = possessed
				}
				NOT = {
					religion = storm_god
				}
			}
		}
		allow = {
			FROM = {
				has_landed_title = c_storms_end
				any_owned_bloodline = {
					has_bloodline_flag = durrandon
				}
				OR = {
					trait = cynical
					trait = arbitrary
					trait = lunatic
					trait = possessed
				}
				NOT = {
					religion = storm_god
				}
			}
		}
		
		effect = {
			hidden_tooltip = {
				religion = storm_god
				FROM = {
					AND = {
						reverse_religion = storm_god
						add_trait = zealous
					}
				}
			}
		}
	}
}