namespace = mbs_old_gods_blessing		

#mbs_old_gods_blessing.1 - Mormont Snowbear
narrative_event = {
	id = mbs_old_gods_blessing.1
	title = "EVTtitlembs_old_gods_blessing.1"
	desc = "EVTDESCmbs_old_gods_blessing.1"
	picture = "GFX_snowbear"
	
	min_age = 14
	
	is_triggered_only = yes
		
	trigger = {
		any_owned_bloodline = {
			has_bloodline_flag = bloodline_mormont
		}
		num_of_children = 3
		any_child  = {
			count = 3
			NOT = { age = 22 }		
			NOT = {
				has_character_flag = blessed_mormont_siblings
			}
			NOT = { 
				trait = snowbear
			}
		}
	}
	
	weight_multiplier = {
		modifier = {
			factor = 5
			NOT = {
				any_dynasty_member = {
					count = 7
					is_alive = yes
					NOT = {
						trait = nightswatch
					}
					NOT = {
						trait = kingsguard
					}
				} 
			}
		}
		modifier = {
			factor = 3
			dynasty = 102
		}
		modifier = {
			factor = 0.4
			NOT = {
				dynasty = 102
			}
		}
		modifier = {
			factor = 1.4
			any_child_even_if_dead = {
				dynasty = ROOT
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 0.4
			any_child_even_if_dead = {
				NOT = {
					dynasty = ROOT
				}
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 3
			trigger = { magic_returned_trigger = yes }
		}
		modifier = {
			factor = 8
			trait = greensight
		}
		modifier = {
			factor = 5
			trait = mystic
		}
		modifier = {
			factor = 1.5
			trigger = { skinchanger_trigger = yes }
		}
		modifier = {
			factor = 1.8
			is_old_gods_religion_trigger = yes
		}
		modifier = {
			factor = 2
			is_old_gods_religion_trigger = yes
			trait = zealous
		}
	}

	option = {
		name = "EVTOPTAmbs_old_gods_blessing.1" #get snowbear
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 3
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 2
				is_old_gods_religion_trigger = yes
			}
			modifier = {
				factor = 0.33
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 0.1
				trait = craven
			}			
			modifier = {
				factor = 3
				trait = brave
			}
			modifier = {
				factor = 10
				trigger = { skinchanger_trigger = yes }
			}
		}
		
		prestige = 15
		
		any_player = {
			limit = { 
				NOT = { character = FROM }
			}
			character_event = { id = mbs_old_gods_blessing_notification.1 }
		}

		random_list = {
			30 = {
				character_event = { id = mbs_old_gods_blessing_parent.1 }
			}
			60 = { }
		}

		any_child  = {
			limit = {
				NOT = { 
					age = 10
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rage.1 days = 45 }
		}

		any_child = {
			limit = {
				NOT = { 
					trait = snowbear
				}
			}	
			character_event = { id = mbs_old_gods_blessing_effect.1 }
		}
	}
	
	option = {
		name = "EVTOPTBmbs_old_gods_blessing.1" #Nope
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 0
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 0.33
				is_old_gods_religion_trigger = yes
				NOT = { trait = cynical }
			}
			modifier = {
				factor = 3
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 3
				trait = cynical
			}
			modifier = {
				factor = 3
				trait = craven
			}
			modifier = {
				factor = 0
				trigger = { skinchanger_trigger = yes }
			}
		}
		any_child = {
			limit = {
				NOT = { 
					trait = snowbear
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rejected.1 }		
		}
	}
}
#mbs_old_gods_blessing.2 - Mormont Brownbear
narrative_event = {
	id = mbs_old_gods_blessing.2
	title = "EVTtitlembs_old_gods_blessing.2"
	desc = "EVTDESCmbs_old_gods_blessing.2"
	picture = "GFX_brownbear"
	
	min_age = 14
	
	is_triggered_only = yes
		
	trigger = {
		any_owned_bloodline = {
			has_bloodline_flag = bloodline_mormont
		}
		num_of_children = 3
		any_child  = {
			count = 3
			NOT = { age = 22 }		
			NOT = {
				has_character_flag = blessed_mormont_siblings
			}
			NOT = { 
				trait = brownbear
			}
		}
	}
	
	weight_multiplier = {
		modifier = {
			factor = 5
			NOT = {
				any_dynasty_member = {
					count = 7
					is_alive = yes
					NOT = {
						trait = nightswatch
					}
					NOT = {
						trait = kingsguard
					}
				} 
			}
		}
		modifier = {
			factor = 3
			dynasty = 102
		}
		modifier = {
			factor = 0.4
			NOT = {
				dynasty = 102
			}
		}
		modifier = {
			factor = 1.4
			any_child_even_if_dead = {
				dynasty = ROOT
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 0.4
			any_child_even_if_dead = {
				NOT = {
					dynasty = ROOT
				}
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 3
			trigger = { magic_returned_trigger = yes }
		}
		modifier = {
			factor = 8
			trait = greensight
		}
		modifier = {
			factor = 5
			trait = mystic
		}
		modifier = {
			factor = 1.5
			trigger = { skinchanger_trigger = yes }
		}
		modifier = {
			factor = 1.8
			is_old_gods_religion_trigger = yes
		}
		modifier = {
			factor = 2
			is_old_gods_religion_trigger = yes
			trait = zealous
		}
	}

	option = {
		name = "EVTOPTAmbs_old_gods_blessing.2" #get snowbear
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 3
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 2
				is_old_gods_religion_trigger = yes
			}
			modifier = {
				factor = 0.33
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 0.1
				trait = craven
			}			
			modifier = {
				factor = 3
				trait = brave
			}
			modifier = {
				factor = 10
				trigger = { skinchanger_trigger = yes }
			}
		}
		
		prestige = 15
		
		any_player = {
			limit = { 
				NOT = { character = FROM }
			}
			character_event = { id = mbs_old_gods_blessing_notification.2 }
		}

		random_list = {
			30 = {
				character_event = { id = mbs_old_gods_blessing_parent.1 }
			}
			60 = { }
		}

		any_child  = {
			limit = {
				NOT = { 
					age = 10
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rage.1 days = 45 }
		}

		any_child = {
			limit = {
				NOT = { 
					trait = brownbear
				}
			}	
			character_event = { id = mbs_old_gods_blessing_effect.2 }
		}
	}
	
	option = {
		name = "EVTOPTBmbs_old_gods_blessing.2" #Nope
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 0
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 0.33
				is_old_gods_religion_trigger = yes
				NOT = { trait = cynical }
			}
			modifier = {
				factor = 3
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 3
				trait = cynical
			}
			modifier = {
				factor = 3
				trait = craven
			}
			modifier = {
				factor = 0
				trigger = { skinchanger_trigger = yes }
			}
		}
		any_child = {
			limit = {
				NOT = { 
					trait = brownbear
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rejected.2 }		
		}
	}
}
#mbs_old_gods_blessing.3 - Blackwood Crow
narrative_event = {
	id = mbs_old_gods_blessing.3
	title = "EVTtitlembs_old_gods_blessing.3"
	desc = "EVTDESCmbs_old_gods_blessing.3"
	picture = "GFX_crow"
	
	min_age = 14
	
	is_triggered_only = yes
		
	trigger = {
		any_owned_bloodline = {
			has_bloodline_flag = bloodline_blackwood
		}
		num_of_children = 3
		any_child  = {
			count = 3
			NOT = { age = 22 }		
			NOT = {
				has_character_flag = blessed_blackwood_siblings
			}
			NOT = { 
				trait = crow
			}
		}
	}
	
	weight_multiplier = {
		modifier = {
			factor = 5
			NOT = {
				any_dynasty_member = {
					count = 7
					is_alive = yes
					NOT = {
						trait = nightswatch
					}
					NOT = {
						trait = kingsguard
					}
				} 
			}
		}
		modifier = {
			factor = 3
			dynasty = 150
		}
		modifier = {
			factor = 0.4
			NOT = {
				dynasty = 150
			}
		}
		modifier = {
			factor = 1.4
			any_child_even_if_dead = {
				dynasty = ROOT
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 0.4
			any_child_even_if_dead = {
				NOT = {
					dynasty = ROOT
				}
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 3
			trigger = { magic_returned_trigger = yes }
		}
		modifier = {
			factor = 8
			trait = greensight
		}
		modifier = {
			factor = 5
			trait = mystic
		}
		modifier = {
			factor = 1.5
			trigger = { skinchanger_trigger = yes }
		}
		modifier = {
			factor = 1.8
			is_old_gods_religion_trigger = yes
		}
		modifier = {
			factor = 2
			is_old_gods_religion_trigger = yes
			trait = zealous
		}
	}

	option = {
		name = "EVTOPTAmbs_old_gods_blessing.3" #get crow
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 3
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 2
				is_old_gods_religion_trigger = yes
			}
			modifier = {
				factor = 0.33
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 0.1
				trait = craven
			}			
			modifier = {
				factor = 3
				trait = brave
			}
			modifier = {
				factor = 10
				trigger = { skinchanger_trigger = yes }
			}
		}
		
		prestige = 15
		
		any_player = {
			limit = { 
				NOT = { character = FROM }
			}
			character_event = { id = mbs_old_gods_blessing_notification.3 }
		}

		random_list = {
			30 = {
				character_event = { id = mbs_old_gods_blessing_parent.3 }
			}
			60 = { }
		}

		any_child  = {
			limit = {
				NOT = { 
					age = 10
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rage.3 days = 45 }
		}

		any_child = {
			limit = {
				NOT = { 
					trait = crow
				}
			}	
			character_event = { id = mbs_old_gods_blessing_effect.3 }
		}
	}
	
	option = {
		name = "EVTOPTBmbs_old_gods_blessing.3" #Nope
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 0
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 0.33
				is_old_gods_religion_trigger = yes
				NOT = { trait = cynical }
			}
			modifier = {
				factor = 3
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 3
				trait = cynical
			}
			modifier = {
				factor = 3
				trait = craven
			}
			modifier = {
				factor = 0
				trigger = { skinchanger_trigger = yes }
			}
		}
		any_child = {
			limit = {
				NOT = { 
					trait = crow
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rejected.3 }		
		}
	}
}
#mbs_old_gods_blessing.4 - Stark Direwolf
narrative_event = {
	id = mbs_old_gods_blessing.4
	title = "EVTtitlembs_old_gods_blessing.4"
	desc = "EVTDESCmbs_old_gods_blessing.4"
	picture = "GFX_wolf"
	
	min_age = 14
	
	is_triggered_only = yes
		
	trigger = {
		any_owned_bloodline = {
			has_bloodline_flag = bloodline_direwolf
		}
		num_of_children = 3
		any_child  = {
			count = 3
			NOT = { age = 22 }		
			NOT = {
				has_character_flag = blessed_stark_siblings
			}
			NOT = { 
				trait = direwolf
			}
		}
	}
	
	weight_multiplier = {
		modifier = {
			factor = 5
			NOT = {
				any_dynasty_member = {
					count = 7
					is_alive = yes
					NOT = {
						trait = nightswatch
					}
					NOT = {
						trait = kingsguard
					}
				} 
			}
		}
		modifier = {
			factor = 3
			dynasty = 59
		}
		modifier = {
			factor = 0.4
			NOT = {
				dynasty = 59
			}
		}
		modifier = {
			factor = 1.4
			any_child_even_if_dead = {
				dynasty = ROOT
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 0.4
			any_child_even_if_dead = {
				NOT = {
					dynasty = ROOT
				}
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 3
			trigger = { magic_returned_trigger = yes }
		}
		modifier = {
			factor = 8
			trait = greensight
		}
		modifier = {
			factor = 5
			trait = mystic
		}
		modifier = {
			factor = 1.5
			trigger = { skinchanger_trigger = yes }
		}
		modifier = {
			factor = 1.8
			is_old_gods_religion_trigger = yes
		}
		modifier = {
			factor = 2
			is_old_gods_religion_trigger = yes
			trait = zealous
		}
	}

	option = {
		name = "EVTOPTAmbs_old_gods_blessing.4" #get direwolf
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 3
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 2
				is_old_gods_religion_trigger = yes
			}
			modifier = {
				factor = 0.33
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 0.1
				trait = craven
			}			
			modifier = {
				factor = 3
				trait = brave
			}
			modifier = {
				factor = 10
				trigger = { skinchanger_trigger = yes }
			}
		}
		
		prestige = 15
		
		any_player = {
			limit = { 
				NOT = { character = FROM }
			}
			character_event = { id = mbs_old_gods_blessing_notification.4 }
		}

		random_list = {
			30 = {
				character_event = { id = mbs_old_gods_blessing_parent.4 }
			}
			60 = { }
		}

		any_child  = {
			limit = {
				NOT = { 
					age = 10
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rage.4 days = 45 }
		}

		any_child = {
			limit = {
				NOT = { 
					trait = direwolf
				}
			}	
			character_event = { id = mbs_old_gods_blessing_effect.4 }
		}
	}
	
	option = {
		name = "EVTOPTBmbs_old_gods_blessing.4" #Nope
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 0
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 0.33
				is_old_gods_religion_trigger = yes
				NOT = { trait = cynical }
			}
			modifier = {
				factor = 3
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 3
				trait = cynical
			}
			modifier = {
				factor = 3
				trait = craven
			}
			modifier = {
				factor = 0
				trigger = { skinchanger_trigger = yes }
			}
		}
		any_child = {
			limit = {
				NOT = { 
					trait = direwolf
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rejected.4 }		
		}
	}
}
#mbs_old_gods_blessing.5 - Marsh King Lizard-Lion
narrative_event = {
	id = mbs_old_gods_blessing.5
	title = "EVTtitlembs_old_gods_blessing.5"
	desc = "EVTDESCmbs_old_gods_blessing.5"
	picture = "GFX_evt_hunting_scene"
	
	min_age = 14
	
	is_triggered_only = yes
		
	trigger = {
		any_owned_bloodline = {
			has_bloodline_flag = bloodline_reed
		}
		num_of_children = 3
		any_child  = {
			count = 3
			NOT = { age = 22 }		
			NOT = {
				has_character_flag = blessed_reed_siblings
			}
			NOT = { 
				trait = lizardlion
			}
		}
	}
	
	weight_multiplier = {
		modifier = {
			factor = 5
			NOT = {
				any_dynasty_member = {
					count = 7
					is_alive = yes
					NOT = {
						trait = nightswatch
					}
					NOT = {
						trait = kingsguard
					}
				} 
			}
		}
		modifier = {
			factor = 3
			dynasty = 126
		}
		modifier = {
			factor = 0.8
			NOT = {
				dynasty = 126
			}
		}
		modifier = {
			factor = 1.4
			any_child_even_if_dead = {
				dynasty = ROOT
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 0.4
			any_child_even_if_dead = {
				NOT = {
					dynasty = ROOT
				}
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 3
			trigger = { magic_returned_trigger = yes }
		}
		modifier = {
			factor = 8
			trait = greensight
		}
		modifier = {
			factor = 5
			trait = mystic
		}
		modifier = {
			factor = 1.5
			trigger = { skinchanger_trigger = yes }
		}
		modifier = {
			factor = 1.8
			is_old_gods_religion_trigger = yes
		}
		modifier = {
			factor = 2
			is_old_gods_religion_trigger = yes
			trait = zealous
		}
	}

	option = {
		name = "EVTOPTAmbs_old_gods_blessing.5" #get lizardlion
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 3
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 2
				is_old_gods_religion_trigger = yes
			}
			modifier = {
				factor = 0.33
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 0.1
				trait = craven
			}			
			modifier = {
				factor = 3
				trait = brave
			}
			modifier = {
				factor = 10
				trigger = { skinchanger_trigger = yes }
			}
		}
		
		prestige = 15
		
		any_player = {
			limit = { 
				NOT = { character = FROM }
			}
			character_event = { id = mbs_old_gods_blessing_notification.5 }
		}

		random_list = {
			30 = {
				character_event = { id = mbs_old_gods_blessing_parent.5 }
			}
			60 = { }
		}

		any_child  = {
			limit = {
				NOT = { 
					age = 10
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rage.5 days = 45 }
		}

		any_child = {
			limit = {
				NOT = { 
					trait = lizardlion
				}
			}	
			character_event = { id = mbs_old_gods_blessing_effect.5 }
		}
	}
	
	option = {
		name = "EVTOPTBmbs_old_gods_blessing.5" #Nope
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 0
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 0.33
				is_old_gods_religion_trigger = yes
				NOT = { trait = cynical }
			}
			modifier = {
				factor = 3
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 3
				trait = cynical
			}
			modifier = {
				factor = 3
				trait = craven
			}
			modifier = {
				factor = 0
				trigger = { skinchanger_trigger = yes }
			}
		}
		any_child = {
			limit = {
				NOT = { 
					trait = lizardlion
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rejected.5 }		
		}
	}
}
#mbs_old_gods_blessing.6 - Banefort Lion
narrative_event = {
	id = mbs_old_gods_blessing.6
	title = "EVTtitlembs_old_gods_blessing.6"
	desc = "EVTDESCmbs_old_gods_blessing.6"
	picture = "GFX_evt_hunting_scene"
	
	min_age = 14
	
	is_triggered_only = yes
		
	trigger = {
		any_owned_bloodline = {
			has_bloodline_flag = bloodline_lion
		}
		num_of_children = 3
		any_child  = {
			count = 3
			NOT = { age = 22 }		
			NOT = {
				has_character_flag = blessed_lion_siblings
			}
			NOT = { 
				trait = lion
			}
		}
	}
	
	weight_multiplier = {
		modifier = {
			factor = 5
			NOT = {
				any_dynasty_member = {
					count = 7
					is_alive = yes
					NOT = {
						trait = nightswatch
					}
					NOT = {
						trait = kingsguard
					}
				} 
			}
		}
		modifier = {
			factor = 3
			dynasty = 200
		}
		modifier = {
			factor = 0.8
			NOT = {
				dynasty = 200
			}
		}
		modifier = {
			factor = 1.4
			any_child_even_if_dead = {
				dynasty = ROOT
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 0.4
			any_child_even_if_dead = {
				NOT = {
					dynasty = ROOT
				}
				NOR = {
					trait = bastard
					trait = legit_bastard
					has_character_flag = bastardised
				}
			}
		}
		modifier = {
			factor = 3
			trigger = { magic_returned_trigger = yes }
		}
		modifier = {
			factor = 1.8
			trait = hunter
		}
		modifier = {
			factor = 8
			trait = greensight
		}
		modifier = {
			factor = 5
			trait = mystic
		}
		modifier = {
			factor = 1.5
			trigger = { skinchanger_trigger = yes }
		}
		modifier = {
			factor = 1.8
			is_old_gods_religion_trigger = yes
		}
		modifier = {
			factor = 2
			is_old_gods_religion_trigger = yes
			trait = zealous
		}
	}

	option = {
		name = "EVTOPTAmbs_old_gods_blessing.6" #get lizardlion
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 3
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 2
				is_old_gods_religion_trigger = yes
			}
			modifier = {
				factor = 0.33
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 0.1
				trait = craven
			}			
			modifier = {
				factor = 3
				trait = brave
			}
			modifier = {
				factor = 10
				trigger = { skinchanger_trigger = yes }
			}
		}
		
		prestige = 15
		
		any_player = {
			limit = { 
				NOT = { character = FROM }
			}
			character_event = { id = mbs_old_gods_blessing_notification.6 }
		}

		random_list = {
			30 = {
				character_event = { id = mbs_old_gods_blessing_parent.6 }
			}
			60 = { }
		}

		any_child  = {
			limit = {
				NOT = { 
					age = 10
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rage.6 days = 45 }
		}

		any_child = {
			limit = {
				NOT = { 
					trait = lizardlion
				}
			}	
			character_event = { id = mbs_old_gods_blessing_effect.6 }
		}
	}
	
	option = {
		name = "EVTOPTBmbs_old_gods_blessing.6" #Nope
		
		ai_chance = {
			factor = 2
						
			modifier = {
				factor = 0
				is_old_gods_religion_trigger = yes
				trait = zealous
			}
			modifier = {
				factor = 0.33
				is_old_gods_religion_trigger = yes
				NOT = { trait = cynical }
			}
			modifier = {
				factor = 3
				NOT = { is_old_gods_religion_trigger = yes }
				trait = zealous
			}
			modifier = {
				factor = 3
				trait = cynical
			}
			modifier = {
				factor = 3
				trait = craven
			}
			modifier = {
				factor = 0
				trigger = { skinchanger_trigger = yes }
			}
		}
		any_child = {
			limit = {
				NOT = { 
					trait = lion
				}
			}	
			character_event = { id = mbs_old_gods_blessing_rejected.6 }		
		}
	}
}