# 1166  - Jiangjin

# County Title
title = c_jiangjin

# Settlements
max_settlements = 4

b_jiangjin_castle = castle
b_jiangjin_city1 = city
b_jiangjin_temple = temple
b_jiangjin_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_jiangjin_castle = ca_asoiaf_yiti_basevalue_1
	b_jiangjin_castle = ca_asoiaf_yiti_basevalue_2

	b_jiangjin_city1 = ct_asoiaf_yiti_basevalue_1

}
	