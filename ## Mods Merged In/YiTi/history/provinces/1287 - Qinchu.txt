# 1190  - Qinchu

# County Title
title = c_qinchu

# Settlements
max_settlements = 4

b_qinchu_castle = castle
b_qinchu_city1 = city
b_qinchu_temple = temple
b_qinchu_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_qinchu_castle = ca_asoiaf_yiti_basevalue_1
	b_qinchu_castle = ca_asoiaf_yiti_basevalue_2

	b_qinchu_city1 = ct_asoiaf_yiti_basevalue_1
	b_qinchu_city1 = ct_asoiaf_yiti_basevalue_2

}
	