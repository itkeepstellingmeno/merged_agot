# 1227  - Zhoufa

# County Title
title = c_zhoufa

# Settlements
max_settlements = 4

b_zhoufa_castle = castle
b_zhoufa_city1 = city
b_zhoufa_temple = temple
b_zhoufa_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_zhoufa_castle = ca_asoiaf_yiti_basevalue_1
	b_zhoufa_castle = ca_asoiaf_yiti_basevalue_2

	b_zhoufa_city1 = ct_asoiaf_yiti_basevalue_1
	b_zhoufa_city1 = ct_asoiaf_yiti_basevalue_2

}
	