# 861  - Minlong

# County Title
title = c_minlong

# Settlements
max_settlements = 6

b_minlong_castle1 = castle
b_minlong_city1 = city
b_minlong_temple = temple
b_minlong_city2 = city
b_minlong_castle2 = city
b_minlong_castle3 = castle

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_minlong_castle1 = ca_asoiaf_yiti_basevalue_1
	b_minlong_castle1 = ca_asoiaf_yiti_basevalue_2
	b_minlong_castle1 = ca_asoiaf_yiti_basevalue_3

	b_minlong_city1 = ct_asoiaf_yiti_basevalue_1
	b_minlong_city1 = ct_asoiaf_yiti_basevalue_2
	b_minlong_city1 = ct_asoiaf_yiti_basevalue_3

	b_minlong_city2 = ct_asoiaf_yiti_basevalue_1
	b_minlong_city2 = ct_asoiaf_yiti_basevalue_2
	b_minlong_city2 = ct_asoiaf_yiti_basevalue_3

}