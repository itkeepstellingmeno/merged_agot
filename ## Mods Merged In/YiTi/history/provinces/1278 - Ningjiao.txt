# 1181  - Ningjiao

# County Title
title = c_ningjiao

# Settlements
max_settlements = 4

b_ningjiao_castle = castle
b_ningjiao_city1 = city
b_ningjiao_temple = temple
b_ningjiao_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_ningjiao_castle = ca_asoiaf_yiti_basevalue_1
	b_ningjiao_castle = ca_asoiaf_yiti_basevalue_2

	b_ningjiao_city1 = ct_asoiaf_yiti_basevalue_1
	b_ningjiao_city1 = ct_asoiaf_yiti_basevalue_2

}
	