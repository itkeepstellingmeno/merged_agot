# 1209  - Leshan

# County Title
title = c_leshan

# Settlements
max_settlements = 4

b_leshan_castle = castle
b_leshan_city1 = city
b_leshan_temple = temple
b_leshan_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_leshan_castle = ca_asoiaf_yiti_basevalue_1
	b_leshan_castle = ca_asoiaf_yiti_basevalue_2

	b_leshan_city1 = ct_asoiaf_yiti_basevalue_1
	b_leshan_city1 = ct_asoiaf_yiti_basevalue_2

}
	