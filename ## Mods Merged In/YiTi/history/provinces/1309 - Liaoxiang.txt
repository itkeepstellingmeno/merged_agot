# 1212  - Liaoxiang

# County Title
title = c_liaoxiang

# Settlements
max_settlements = 4

b_liaoxiang_castle = castle
b_liaoxiang_city1 = city
b_liaoxiang_temple = temple
b_liaoxiang_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_liaoxiang_castle = ca_asoiaf_yiti_basevalue_1
	b_liaoxiang_castle = ca_asoiaf_yiti_basevalue_2

	b_liaoxiang_city1 = ct_asoiaf_yiti_basevalue_1
	b_liaoxiang_city1 = ct_asoiaf_yiti_basevalue_2

}
	