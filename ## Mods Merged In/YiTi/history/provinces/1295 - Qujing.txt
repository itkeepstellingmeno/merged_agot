# 1198  - Qujing

# County Title
title = c_qujing

# Settlements
max_settlements = 4

b_qujing_castle = castle
b_qujing_city1 = city
b_qujing_temple = temple
b_qujing_city2 = city

# Misc
culture = yi_ti
religion = yiti_gods

# History
1.1.1 = {

	b_qujing_castle = ca_asoiaf_yiti_basevalue_1
	b_qujing_castle = ca_asoiaf_yiti_basevalue_2

	b_qujing_city1 = ct_asoiaf_yiti_basevalue_1
	b_qujing_city1 = ct_asoiaf_yiti_basevalue_2

}
	