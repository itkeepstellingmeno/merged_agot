hyrkoon_group = {
	graphical_cultures = { mesoamericangfx } # portraits, units

	hyrkooni = {
		graphical_cultures = { bonemountainsgfx }
		
		color = { 0.95 0.35 0.3 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			Hyrkoon:1500
			
			Aaro Ahvo Armas Auvo Antto Aslak Aulis
			Dyrvid
			Eero Elmeri Esko
			Hyrkko Hyrno Hannu
			Ikka Isko Iyvari
			Jyri Jyrki Jarkko Juha Jouko
			Kalevoon Kain Kari Kyosti
			Lynnart
			Markku Mykoon Myikku
			Nyilo Nyyrikki
			Oiva Orvo
			Pyrkoon Pekko Pyrkko Panu
			Ryku Raimo Raitis
			Sakari Soini Saku Santtu
			Taito Tero Tuukka Touko Tarvo
			Usko Uljas
			Veikko Voitto Valto
			
			#DAMOC
			Aranti Ampuja Asikka Artukka Aikamieli Aikio Ano Arijoutsi Arpia Auvo Armas Arvo Asikko Ensio Eura Himottu Hirvi Havu Harmaa Haukka 
			Huuhka Hyvari Hyvalempi Hyvatty Harkapaa Hintsa Ihanti Ihalempi Ihamuoto Ikavalko Ilakka Ilma Ilmatoivia Ikitiera Ikopaiva Ilmo Into Jalkava Jurva
			Kaleva Kaivattu Kainu Kaipia Koveri Kulta Kupias Kaukovalta Keiho Kekko Kokko Kostia Kotarikko Kultamies Kova Kukko Kurittu Kalevi Kettu Kolli 
			Lintu Lemetti Lempi Leino Lemmas Lempo Maanavilja Montaja Menikko Mustia Miero Mielikko Mielitty Mietti Miemo Miela Nousia Neuvo Nuolia Ohto Ora Otra Osmoon Oivas Otava Otso 
			Paaso Puukko Pitkapaa Pitka Paivio Rautio Rahikka Reko Repo Saukko Suorsa Satajalka Satatieto Sotijalo Seppo Susi Torittu Tyrkoon Hyro Byrkoon Jyrkoon
			Terho Tapatora Tammi Tapavaino Tietava Tornio Toivas Tulo Titti Toikka Toivo Tuokki Torio Unaja Uro Utujoutsi Ukko Uoti Urho Vaino Vaania Vaito Vartia Valta Valtari Vihavaino Viljakka Valto Veli Viti Villi Voitto
		
		}
		female_names = {
			Aava Aulikki Aura Annukka Arja Aynikki Ayri
			Elviira Esteri Eyni Eeva Eerika Erja
			Fynni
			Hylkka Hyrlli Hellevi Hyrnna Hyrkka
			Inka Iyta Inkeri
			Janika Jutta
			Kyllikki Kyrtta Kynerva Kyira Kylli
			Lyyti Lumikki Laimi Loviisa
			Myrva Marjukka Maija Maila Minea Myrka
			Nelli Naima Noora
			Orvokki Onyrva 
			Pyrkka Pilvi Pirja
			Rauna Rykka Rytva Raisa
			Sylvi Syrkka Syrpa Saima Sari Soila Suvi
			Tyrtta Taava Tekla Tari
			
			#DAMOC
			Aamu Aino Hella Helmi Hellikki Hopea Ilta Kaarina Kapy Kielo Kukka Kuutamo Lapsa Lauha Lempi Liekko Mahla Maija Marja Mesi Metta Oiva Paiva Paivi Pihla Pilvi Pyry Rauha Raita Sati
			Sisko Suoma Salme Taimi Talvikki Terhi Tuija Tuovi Tuuli Tuulikki Tuomikki Tyyne Tyyni Vanamo Venla Hyra	
			Anava Ashava Asrava Avya Azra-Va Chechama Chenksa Cherava Chichayka Chindyapa Chindyayka Chinzhay Chitska Elyuva Inyava Kanyava Kanyuva
			Kirdyava Kunava Kunyavka Litava Liyava Mazava Megurka Mel'shay Nezayka Nulzyava Nyal'ka Nyayka Nyumina Nyv Olota Ordava Oshama oshkamoshka Parava Pekshayka
			Pemka Pokshava Poshayka Potekay Potyava Putyayka Ravzhava Rayda Ruzava Sanal'ka Sangasa Sernyava Serzhay Setyamka Seyamka Shochynava Shonzhava
			Shukshtoroyka Sinyava Siyamka Slyugan'ka Sochava Styaka Syryava Tatka Tekay Tetyava Tikshayka Toyaksha Tundava Tundya Unzhutka Upurga
			Utyayka Valdava Vergava Vezhav Vidyava Virtyava Yalgava Yugorka	 Hyrkoona:500
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
	
	kayakayanayan = {
		graphical_cultures = { bonemountainsgfx }
		
		color = { 0.95 0.35 0.3 }

		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			Hyrkoon:1500
			
			Aaro Ahvo Armas Auvo Antto Aslak Aulis
			Dyrvid
			Eero Elmeri Esko
			Hyrkko Hyrno Hannu
			Ikka Isko Iyvari
			Jyri Jyrki Jarkko Juha Jouko
			Kalevoon Kain Kari Kyosti
			Lynnart
			Markku Mykoon Myikku
			Nyilo Nyyrikki
			Oiva Orvo
			Pyrkoon Pekko Pyrkko Panu
			Ryku Raimo Raitis
			Sakari Soini Saku Santtu
			Taito Tero Tuukka Touko Tarvo
			Usko Uljas
			Veikko Voitto Valto
			
			#DAMOC
			Aranti Ampuja Asikka Artukka Aikamieli Aikio Ano Arijoutsi Arpia Auvo Armas Arvo Asikko Ensio Eura Himottu Hirvi Havu Harmaa Haukka 
			Huuhka Hyvari Hyvalempi Hyvatty Harkapaa Hintsa Ihanti Ihalempi Ihamuoto Ikavalko Ilakka Ilma Ilmatoivia Ikitiera Ikopaiva Ilmo Into Jalkava Jurva
			Kaleva Kaivattu Kainu Kaipia Koveri Kulta Kupias Kaukovalta Keiho Kekko Kokko Kostia Kotarikko Kultamies Kova Kukko Kurittu Kalevi Kettu Kolli 
			Lintu Lemetti Lempi Leino Lemmas Lempo Maanavilja Montaja Menikko Mustia Miero Mielikko Mielitty Mietti Miemo Miela Nousia Neuvo Nuolia Ohto Ora Otra Osmoon Oivas Otava Otso 
			Paaso Puukko Pitkapaa Pitka Paivio Rautio Rahikka Reko Repo Saukko Suorsa Satajalka Satatieto Sotijalo Seppo Susi Torittu Tyrkoon Hyro Byrkoon Jyrkoon
			Terho Tapatora Tammi Tapavaino Tietava Tornio Toivas Tulo Titti Toikka Toivo Tuokki Torio Unaja Uro Utujoutsi Ukko Uoti Urho Vaino Vaania Vaito Vartia Valta Valtari Vihavaino Viljakka Valto Veli Viti Villi Voitto
		
		}
		female_names = {
			Aava Aulikki Aura Annukka Arja Aynikki Ayri
			Elviira Esteri Eyni Eeva Eerika Erja
			Fynni
			Hylkka Hyrlli Hellevi Hyrnna Hyrkka
			Inka Iyta Inkeri
			Janika Jutta
			Kyllikki Kyrtta Kynerva Kyira Kylli
			Lyyti Lumikki Laimi Loviisa
			Myrva Marjukka Maija Maila Minea Myrka
			Nelli Naima Noora
			Orvokki Onyrva 
			Pyrkka Pilvi Pirja
			Rauna Rykka Rytva Raisa
			Sylvi Syrkka Syrpa Saima Sari Soila Suvi
			Tyrtta Taava Tekla Tari
			
			#DAMOC
			Aamu Aino Hella Helmi Hellikki Hopea Ilta Kaarina Kapy Kielo Kukka Kuutamo Lapsa Lauha Lempi Liekko Mahla Maija Marja Mesi Metta Oiva Paiva Paivi Pihla Pilvi Pyry Rauha Raita Sati
			Sisko Suoma Salme Taimi Talvikki Terhi Tuija Tuovi Tuuli Tuulikki Tuomikki Tyyne Tyyni Vanamo Venla Hyra	
			Anava Ashava Asrava Avya Azra-Va Chechama Chenksa Cherava Chichayka Chindyapa Chindyayka Chinzhay Chitska Elyuva Inyava Kanyava Kanyuva
			Kirdyava Kunava Kunyavka Litava Liyava Mazava Megurka Mel'shay Nezayka Nulzyava Nyal'ka Nyayka Nyumina Nyv Olota Ordava Oshama oshkamoshka Parava Pekshayka
			Pemka Pokshava Poshayka Potekay Potyava Putyayka Ravzhava Rayda Ruzava Sanal'ka Sangasa Sernyava Serzhay Setyamka Seyamka Shochynava Shonzhava
			Shukshtoroyka Sinyava Siyamka Slyugan'ka Sochava Styaka Syryava Tatka Tekay Tetyava Tikshayka Toyaksha Tundava Tundya Unzhutka Upurga
			Utyayka Valdava Vergava Vezhav Vidyava Virtyava Yalgava Yugorka	Hyrkoona:500	
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
	
	samyrianan = {
		graphical_cultures = { bonemountainsgfx }
		
		color = { 0.9 0.55 0.34 } 
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			Hyrkoon:1500
			
			Aaro Ahvo Armas Auvo Antto Aslak Aulis
			Dyrvid
			Eero Elmeri Esko
			Hyrkko Hyrno Hannu
			Ikka Isko Iyvari
			Jyri Jyrki Jarkko Juha Jouko
			Kalevoon Kain Kari Kyosti
			Lynnart
			Markku Mykoon Myikku
			Nyilo Nyyrikki
			Oiva Orvo
			Pyrkoon Pekko Pyrkko Panu
			Ryku Raimo Raitis
			Sakari Soini Saku Santtu
			Taito Tero Tuukka Touko Tarvo
			Usko Uljas
			Veikko Voitto Valto
			
			#DAMOC
			Aranti Ampuja Asikka Artukka Aikamieli Aikio Ano Arijoutsi Arpia Auvo Armas Arvo Asikko Ensio Eura Himottu Hirvi Havu Harmaa Haukka 
			Huuhka Hyvari Hyvalempi Hyvatty Harkapaa Hintsa Ihanti Ihalempi Ihamuoto Ikavalko Ilakka Ilma Ilmatoivia Ikitiera Ikopaiva Ilmo Into Jalkava Jurva
			Kaleva Kaivattu Kainu Kaipia Koveri Kulta Kupias Kaukovalta Keiho Kekko Kokko Kostia Kotarikko Kultamies Kova Kukko Kurittu Kalevi Kettu Kolli 
			Lintu Lemetti Lempi Leino Lemmas Lempo Maanavilja Montaja Menikko Mustia Miero Mielikko Mielitty Mietti Miemo Miela Nousia Neuvo Nuolia Ohto Ora Otra Osmoon Oivas Otava Otso 
			Paaso Puukko Pitkapaa Pitka Paivio Rautio Rahikka Reko Repo Saukko Suorsa Satajalka Satatieto Sotijalo Seppo Susi Torittu Tyrkoon Hyro Byrkoon Jyrkoon
			Terho Tapatora Tammi Tapavaino Tietava Tornio Toivas Tulo Titti Toikka Toivo Tuokki Torio Unaja Uro Utujoutsi Ukko Uoti Urho Vaino Vaania Vaito Vartia Valta Valtari Vihavaino Viljakka Valto Veli Viti Villi Voitto
		
		}
		female_names = {
			Aava Aulikki Aura Annukka Arja Aynikki Ayri
			Elviira Esteri Eyni Eeva Eerika Erja
			Fynni
			Hylkka Hyrlli Hellevi Hyrnna Hyrkka
			Inka Iyta Inkeri
			Janika Jutta
			Kyllikki Kyrtta Kynerva Kyira Kylli
			Lyyti Lumikki Laimi Loviisa
			Myrva Marjukka Maija Maila Minea Myrka
			Nelli Naima Noora
			Orvokki Onyrva 
			Pyrkka Pilvi Pirja
			Rauna Rykka Rytva Raisa
			Sylvi Syrkka Syrpa Saima Sari Soila Suvi
			Tyrtta Taava Tekla Tari
			
			#DAMOC
			Aamu Aino Hella Helmi Hellikki Hopea Ilta Kaarina Kapy Kielo Kukka Kuutamo Lapsa Lauha Lempi Liekko Mahla Maija Marja Mesi Metta Oiva Paiva Paivi Pihla Pilvi Pyry Rauha Raita Sati
			Sisko Suoma Salme Taimi Talvikki Terhi Tuija Tuovi Tuuli Tuulikki Tuomikki Tyyne Tyyni Vanamo Venla Hyra	
			Anava Ashava Asrava Avya Azra-Va Chechama Chenksa Cherava Chichayka Chindyapa Chindyayka Chinzhay Chitska Elyuva Inyava Kanyava Kanyuva
			Kirdyava Kunava Kunyavka Litava Liyava Mazava Megurka Mel'shay Nezayka Nulzyava Nyal'ka Nyayka Nyumina Nyv Olota Ordava Oshama oshkamoshka Parava Pekshayka
			Pemka Pokshava Poshayka Potekay Potyava Putyayka Ravzhava Rayda Ruzava Sanal'ka Sangasa Sernyava Serzhay Setyamka Seyamka Shochynava Shonzhava
			Shukshtoroyka Sinyava Siyamka Slyugan'ka Sochava Styaka Syryava Tatka Tekay Tetyava Tikshayka Toyaksha Tundava Tundya Unzhutka Upurga
			Utyayka Valdava Vergava Vezhav Vidyava Virtyava Yalgava Yugorka	Hyrkoona:500	
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
	
	bayasabhadi = {
		graphical_cultures = { bonemountainsgfx }
		
		color = { 0.87 0.75 0.38 } 
		
		alternate_start = { NOT = { has_alternate_start_parameter = { key = special_culture value = animal_world } } }
		
		male_names = {
			Hyrkoon:1500
			
			Aaro Ahvo Armas Auvo Antto Aslak Aulis
			Dyrvid
			Eero Elmeri Esko
			Hyrkko Hyrno Hannu
			Ikka Isko Iyvari
			Jyri Jyrki Jarkko Juha Jouko
			Kalevoon Kain Kari Kyosti
			Lynnart
			Markku Mykoon Myikku
			Nyilo Nyyrikki
			Oiva Orvo
			Pyrkoon Pekko Pyrkko Panu
			Ryku Raimo Raitis
			Sakari Soini Saku Santtu
			Taito Tero Tuukka Touko Tarvo
			Usko Uljas
			Veikko Voitto Valto
			
			#DAMOC
			Aranti Ampuja Asikka Artukka Aikamieli Aikio Ano Arijoutsi Arpia Auvo Armas Arvo Asikko Ensio Eura Himottu Hirvi Havu Harmaa Haukka 
			Huuhka Hyvari Hyvalempi Hyvatty Harkapaa Hintsa Ihanti Ihalempi Ihamuoto Ikavalko Ilakka Ilma Ilmatoivia Ikitiera Ikopaiva Ilmo Into Jalkava Jurva
			Kaleva Kaivattu Kainu Kaipia Koveri Kulta Kupias Kaukovalta Keiho Kekko Kokko Kostia Kotarikko Kultamies Kova Kukko Kurittu Kalevi Kettu Kolli 
			Lintu Lemetti Lempi Leino Lemmas Lempo Maanavilja Montaja Menikko Mustia Miero Mielikko Mielitty Mietti Miemo Miela Nousia Neuvo Nuolia Ohto Ora Otra Osmoon Oivas Otava Otso 
			Paaso Puukko Pitkapaa Pitka Paivio Rautio Rahikka Reko Repo Saukko Suorsa Satajalka Satatieto Sotijalo Seppo Susi Torittu Tyrkoon Hyro Byrkoon Jyrkoon
			Terho Tapatora Tammi Tapavaino Tietava Tornio Toivas Tulo Titti Toikka Toivo Tuokki Torio Unaja Uro Utujoutsi Ukko Uoti Urho Vaino Vaania Vaito Vartia Valta Valtari Vihavaino Viljakka Valto Veli Viti Villi Voitto
		
		}
		female_names = {
			Aava Aulikki Aura Annukka Arja Aynikki Ayri
			Elviira Esteri Eyni Eeva Eerika Erja
			Fynni
			Hylkka Hyrlli Hellevi Hyrnna Hyrkka
			Inka Iyta Inkeri
			Janika Jutta
			Kyllikki Kyrtta Kynerva Kyira Kylli
			Lyyti Lumikki Laimi Loviisa
			Myrva Marjukka Maija Maila Minea Myrka
			Nelli Naima Noora
			Orvokki Onyrva 
			Pyrkka Pilvi Pirja
			Rauna Rykka Rytva Raisa
			Sylvi Syrkka Syrpa Saima Sari Soila Suvi
			Tyrtta Taava Tekla Tari
			
			#DAMOC
			Aamu Aino Hella Helmi Hellikki Hopea Ilta Kaarina Kapy Kielo Kukka Kuutamo Lapsa Lauha Lempi Liekko Mahla Maija Marja Mesi Metta Oiva Paiva Paivi Pihla Pilvi Pyry Rauha Raita Sati
			Sisko Suoma Salme Taimi Talvikki Terhi Tuija Tuovi Tuuli Tuulikki Tuomikki Tyyne Tyyni Vanamo Venla Hyra	
			Anava Ashava Asrava Avya Azra-Va Chechama Chenksa Cherava Chichayka Chindyapa Chindyayka Chinzhay Chitska Elyuva Inyava Kanyava Kanyuva
			Kirdyava Kunava Kunyavka Litava Liyava Mazava Megurka Mel'shay Nezayka Nulzyava Nyal'ka Nyayka Nyumina Nyv Olota Ordava Oshama oshkamoshka Parava Pekshayka
			Pemka Pokshava Poshayka Potekay Potyava Putyayka Ravzhava Rayda Ruzava Sanal'ka Sangasa Sernyava Serzhay Setyamka Seyamka Shochynava Shonzhava
			Shukshtoroyka Sinyava Siyamka Slyugan'ka Sochava Styaka Syryava Tatka Tekay Tetyava Tikshayka Toyaksha Tundava Tundya Unzhutka Upurga
			Utyayka Valdava Vergava Vezhav Vidyava Virtyava Yalgava Yugorka	Hyrkoona:500	
		}
		from_dynasty_prefix = "of "
		
		# Chance of male children being named after their paternal or maternal grandfather, or their father. Sum must not exceed 100.
		pat_grf_name_chance = 40
		mat_grf_name_chance = 0
		father_name_chance = 0
		
		# Chance of female children being named after their paternal or maternal grandmother, or their mother. Sum must not exceed 100.
		pat_grm_name_chance = 10
		mat_grm_name_chance = 30
		mother_name_chance = 0
		
		modifier = default_culture_modifier
	}
}

