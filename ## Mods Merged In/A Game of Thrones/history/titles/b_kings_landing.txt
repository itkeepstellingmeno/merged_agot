700.1.1 = {
	name = b_blackwater
}

8000.1.1 = {
	liege="c_kings_landing"
	law = succ_appointment
	reset_name = yes
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government_city 
				PREV = { succession = appointment }
				recalc_succession = yes
				give_minor_title = title_commander_gold_cloaks
			}
		}
	}
}

#Since Qarl Corbray was lord, his office is ommited (8048.3.7 - 8059.2.1)

8059.2.1 = {
	holder = 16288 #Robert Redwyne
}
8089.1.1 = {
	holder = 0
}
8105.1.1 = {
	holder=779496 #Daemon Targaryen
}
8106.1.1={
	holder = 0
}
8120.1.1={
	holder = 30509006 #Luthor Largent
}
8130.5.22={ 
	holder = 0	#Balon Byrch ommited, as he was Captain for Rhaenyra and she is never holder of e_iron_throne in our mod
}
8133.8.1={
	holder=6012299 #Lucas Leygood
}
8136.1.1={
	holder=6001430 #Adrian Thorne
}
8160.1.1={
	holder=6001033 #Richard Harte
}
8165.1.1={
	holder=220156 # Luthor Rivers
}
8196.6.1={
	holder=0
}
8275.1.1={
	holder = 77103 #Manly Stokeworth
}
8283.6.1={
	holder = 200353 #Janos Slynt
}
8298.11.2={
	holder = 2033045 #Jacelyn Bywater
}
8299.7.2 = {
	holder = 1212 #Addam Marbrand
}
8300.2.9 = {
	holder = 202352 #Osfryd Kettleback
}
8300.5.17 = {
	holder = 696969106 #Humfrey Waters
}



