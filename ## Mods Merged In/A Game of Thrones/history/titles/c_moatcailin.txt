6000.1.1={
	liege="e_north"
	law = succ_appointment
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}	
}
7988.1.1={
	holder = 20059 #Torrhen Stark
}	
7999.1.1 = {	
	liege="k_north"
}
8014.1.1 = {
	holder=20159 #Brandon Stark
}
8049.10.1 = {
	holder=40259 #Walton Stark 
}
8050.1.1 = {
	holder=552059 #Alaric Stark
}
8072.1.1 = {
	holder=30359 #Edric Stark
}
8097.1.1 = {
	holder = 20259 #Ellard Stark
}
8106.1.1 = {
	holder = 20559 #Benjen Stark
}
8115.1.1 = {
	holder=20659 # Rickon Stark 
}
8121.1.1 = {
	holder=20859 #Cregan Stark 
}
8173.1.1 = {
	holder=22059 #Jonnel Stark 
}
8184.1.1 ={
	holder=24059 #Barthogan 'Barth' Stark 
}
8195.6.1 ={
	holder=25059 #Brandon Stark
}
8198.1.1 = {
	holder=28059 # Rodwell  Stark
}
8203.1.1 = {
	holder=21059 # Beron  Stark
}
8212.1.1 = {
	holder=26059 #Donnor Stark {Beron's 1st son}
}
8213.1.1 = {
	holder=20959 #Willam Stark {Beron's 2nd son}
}
8226.9.9 = {
	holder=21169 #Edwyle Stark {Willem's son}
}
8262.1.1 = {
	holder=2058 #Rickard Stark
}
8282.1.1={
	holder=59 #Eddard Stark
}
8298.11.2={
	holder = 2059 #Robb Stark
	#liege=0
}

8298.12.20 = {
	liege = "e_north"
}
8299.11.5={
	holder = 87 #roose bolton
	liege="k_north"
}
