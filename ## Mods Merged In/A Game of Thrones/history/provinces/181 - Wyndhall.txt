# 181 - Wyndhall

# County Title
title = c_wyndhall

# Settlements
max_settlements = 5
b_wyndhall = castle
b_wyndhalltown = city
b_westford = castle
b_wyndhallsept = city

# Misc
culture = old_first_man
religion = old_gods

#History

1.1.1 = {
	b_wyndhall = ca_asoiaf_westerland_basevalue_1
	b_wyndhall = ca_asoiaf_westerland_basevalue_2
	b_wyndhall = ca_asoiaf_westerland_basevalue_3
	#b_wyndhall = ca_asoiaf_westerland_basevalue_3

	b_wyndhalltown = ct_asoiaf_westerland_basevalue_1
	b_wyndhalltown = ct_asoiaf_westerland_basevalue_2
	b_wyndhalltown = ct_asoiaf_westerland_basevalue_1

	b_westford = ca_asoiaf_westerland_basevalue_1
	b_westford = ca_asoiaf_westerland_basevalue_2
	b_westford = ca_asoiaf_westerland_basevalue_3
	
	b_wyndhallsept = ct_asoiaf_westerland_basevalue_1
	b_wyndhallsept = ct_asoiaf_westerland_basevalue_2
}
6700.1.1 = {
	culture = westerman
	religion = the_seven
}