decisions = {
	# Male ruler can take the black and abdicates.
	abdicate_to_the_nights_watch_player = {
		ai = no
		is_high_prio = yes
		only_playable = yes
		only_rulers = yes

		potential = {
			is_adult = yes
			is_female = no
			is_married = no
			is_ruler = yes
			NOT = {
				trait = deserter
				trait = nightswatch
				trait = wildling
			}
		}

		allow = {
			d_nightswatch = { has_holder = yes }
			capital_scope = {
				OR = {
					region = world_westeros
					region = world_stepstones
					region = world_free_cities
					region = world_valyrian_peninsula
				}
			}
			ROOT = {
				current_heir = {
					dynasty = ROOT
					age >= 16
				}
			}
			war = no
		}

		effect = {
			character_event = {	id = abdicateblack.1 }
			custom_tooltip = { text = abdicate_to_the_nights_watch_player_tooltip }
		}
	}
}