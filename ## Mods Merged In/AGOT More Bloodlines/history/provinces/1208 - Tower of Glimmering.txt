# 1159 - Tower of Glimmering

# County Title
title = c_tower_of_glimmering

# Settlements
max_settlements = 1

b_tower_of_glimmering_mbs = castle

# Misc
culture = old_ironborn
religion = drowned_god

#History

1.1.1 = {b_tower_of_glimmering_mbs = ca_asoiaf_ironislands_basevalue_1}
1.1.1 = {b_tower_of_glimmering_mbs = ca_asoiaf_ironislands_basevalue_2}

6700.1.1 = {
	culture = ironborn
}

