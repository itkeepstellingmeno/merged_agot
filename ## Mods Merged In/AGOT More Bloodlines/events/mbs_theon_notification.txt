namespace = mbs_theon_notification

#mbs_theon_notification.1 - Theon Defects General
narrative_event = {
	id = mbs_theon_notification.1
	title = "EVTtitlembs_theon_notification.1"
	desc = "EVTDESCmbs_theon_notification.1"
	picture = "GFX_GREYJOY"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_theon_notification.1" #
		
		ai_chance = {
			factor = 2
		}		
		e_north = { holder_scope = { tooltip = { show_portrait = yes } } }
		e_iron_isles = { holder_scope = { tooltip = { show_portrait = yes } } }
	}
}
#mbs_theon_notification.2 - Theon Defects Balon
narrative_event = {
	id = mbs_theon_notification.2
	title = "EVTtitlembs_theon_notification.2"
	desc = "EVTDESCmbs_theon_notification.2"
	picture = "GFX_GREYJOY"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_theon_notification.2" #
		
		ai_chance = {
			factor = 2
		}		
		e_north = { holder_scope = { tooltip = { show_portrait = yes } } }
		e_iron_isles = { holder_scope = { tooltip = { show_portrait = yes } } }
	}
}
#mbs_theon_notification.3 - Theon Defects Robb
narrative_event = {
	id = mbs_theon_notification.3
	title = "EVTtitlembs_theon_notification.3"
	desc = "EVTDESCmbs_theon_notification.3"
	picture = "GFX_GREYJOY"
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAmbs_theon_notification.3" #
		
		ai_chance = {
			factor = 2
		}		
		e_north = { holder_scope = { tooltip = { show_portrait = yes } } }
		e_iron_isles = { holder_scope = { tooltip = { show_portrait = yes } } }
	}
}
