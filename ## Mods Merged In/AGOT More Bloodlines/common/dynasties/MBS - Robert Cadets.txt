
1074 = {
	name="Stonehind"
	culture = valeman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 37
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1075 = {
	name="Stonebell"
	culture = riverlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 38
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1076 = {
	name = "Bullhart"
	culture = stormlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 23
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1077 = {
	name = "Robhart"
	culture = crownlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 24
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1078 = {
	name= "Goldstag"
	culture = westerman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 4
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1079 = {
	name= "Goldstag"
	culture = westerman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 4
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1080 = {
	name= "Seastag"
	culture = ironborn
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 5
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1081 = {
	name= "Stormhaven"
	culture = stormlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 6
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1082 = {
	name="Hartford"
	culture = riverlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 25
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1083 = {
	name="Coldhart"
	culture = northman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 20
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1084 = {
	name="Redfawn"
	culture = stone_dornish
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 21
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1085 = {
	name="Baarthreen"
	culture = stormlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 22
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1086 = {
	name="Stagsby"
	culture = stormlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 7
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1087 = {
	name="Darkhart"
	culture = riverlander
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 8
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}
1088 = {
	name="Warfield"
	culture = reachman
	coat_of_arms = {
		template = 0
		layer = {
			texture = 22
			texture_internal = 26
			emblem = 0
			color = 0
			color = 0
			color = 0
		}
	}
	used_for_random = no
	can_appear = no
}