1.1.1={
	liege="k_southstoneTK"
	law = succ_primogeniture
}
3010.1.1={
	holder = 1030128 # Blackwood King
}	
3054.1.1 = {
	holder = 0
}
7600.1.1={
	liege="e_riverlands"
	law = succ_primogeniture
	name = c_stonehedge
	effect = {
		location = { 
			set_name = c_stonehedge
		}
	}
}
6854.1.1={holder=61000149} # Robert Bracken
6907.8.1={holder=0}
7877.1.1={holder=60400149} # Benedict Bracken
7916.8.1={holder=60300149} # Lothar Bracken (C)
7933.6.1={holder=60200149} # Otho Bracken
7952.1.1={holder=60000149} # Jonos Bracken
7986.1.1={holder=22149} # Addam Bracken

7998.2.1={
	liege="k_riverlands"
}
8013.1.1={
	holder=21149
}
8049.1.1={
	holder=19149
}
8073.1.1={
	holder=18149
}
8102.1.1={
	holder=17149
}
8137.1.1={
	holder=16149
}
8163.1.1={
	holder=15149
}
8178.6.1={
	holder=11149
}
8195.3.7={
	liege = 0
}
8196.6.1={
	liege = "k_riverlands"
}
8214.1.1={
	holder=9149
}
8243.1.1={
	holder=8149
}
8278.1.1={
	holder=149
}
