1.1.1={ law=succ_primogeniture law=cognatic_succession
	liege="d_oldtown"
}
25.1.1={holder=6000285} # Uthor of the High tower (C)
94.1.1={holder=6100285} # Urrigon (C)
120.1.1={holder=6200285} # Otho I (C)
140.1.1={holder=6300285} # Uthor II
175.1.1={holder=6400285} # Otho II (C)
195.1.1={holder=0}
2670.1.1={holder=6700285} # Lymond the Sea Lion # Last king of the Oldtown (C)
2723.1.1={holder=0}

6350.1.1={holder=6900285} # Jeremy (C)
6395.1.1={holder=61000285} # Jason (C)
6415.1.1={holder=61100285} # Jared
6445.1.1={holder=61200285} # Jarett
6490.1.1={holder=61300285} # Lymond II
6540.1.1={holder=61400285} # John
6555.1.1={holder=61500285} # Otto
6585.1.1={holder=61600285} # Gareth
6605.1.1={holder=611700285} # Dorian (C)
6638.1.1={holder=61800285} # Ormond 
6650.1.1={holder=61900285} # Damon the Devout (C) # First to convert to the seven
6665.1.1={holder=62000285} # Triston (C)
6715.1.1={holder=62100285} # Barris (C)