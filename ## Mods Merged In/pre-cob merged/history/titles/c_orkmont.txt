1.1.1={
	liege="e_iron_isles"
	law = agnatic_succession
	law = succ_primogeniture
}
2917.1.1 = { holder = 500174420 } #Urras  Greyiron
2942.1.1 = { holder = 501174420 } #Erich I Greyiron
2943.1.1 = { holder = 0 } 
2956.1.1 = { holder = 502174420 } #Theon I Greyiron (NC)
2993.1.1 = { holder = 0 } 
3009.1.1 = { holder = 503174420 } #Balon I Greyiron (NC)
3034.1.1 = { holder = 0 }

3487.1.1 = { holder = 355066 } #Harrag  Hoare
3519.1.1 = { holder = 357066 } #Erich II Hoare
3545.1.1 = { holder = 504174420 } #Urrathon I Greyiron (NC)
3556.1.1 = { holder = 505174420 } #Urrathon II Greyiron (NC)
3578.1.1 = { holder = 0 } 
3638.1.1 = { holder = 506174420 } #Urragon I Greyiron (NC)
3658.1.1 = { holder = 0 } 
3670.1.1 = { holder = 358066 } #Qhored  Hoare
3745.1.1 = { holder = 0 }

4159.1.1 = { holder = 507174420 } #Balon III Greyiron (NC)
4177.1.1 = { holder = 508174420 } #Erich III Greyiron (NC)
4216.1.1 = { holder = 509174420 } #Rognar I Greyiron (NC)
4230.1.1 = { holder = 0 } 
4238.1.1 = { holder = 510174420 } #Erich IV Greyiron (NC)
4278.1.1 = { holder = 511174420 } #Urrathon III Greyiron (NC)
4287.1.1 = { holder = 0 } 

5014.1.1 = { holder = 524174420 } #Urragon II Greyiron (NC)
5023.1.1 = { holder = 0 } 
5059.1.1 = { holder = 512174420 } #Urragon III Greyiron
5087.1.1 = { holder = 0 } 
5089.1.1 = { holder = 513174420 } #Torgon  Greyiron
5129.1.1 = { holder = 517174420 } #Urragon IV Greyiron

5181.1.1 = { 
	holder = 520174420  #Urron  Greyiron
	law = succ_primogeniture
} 
5203.1.1 = { holder = 0 }

6478.1.1 = { holder = 525174420 } #Joron II Greyiron (NC)
6521.1.1 = { holder = 521174420 } #Euron I Greyiron (NC)
6540.1.1 = { holder = 522174420 } #Balon VII Greyiron (NC)
6563.1.1 = { holder = 523174420 } #Rognar II Greyiron

6584.1.1 = { holder = 114116 }
6626.1.1 = { holder = 121116 }
6634.1.1 = { holder = 123116 }
6639.1.1 = { holder = 124116 }
6658.1.1 = { holder = 128116 }
6687.1.1 = { holder = 129116 }
6692.1.1 = { holder = 135116 }

