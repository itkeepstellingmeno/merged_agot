1.1.1={
	law = succ_primogeniture
	law = true_cognatic_succession
	liege = d_velvet_hills
}

6473.1.1 = { holder=1000008 } # Rycas (nc)
6500.1.1 = { holder=1001008 } # Bors (nc)
6529.1.1 = { holder=1003008 } # Caleyen (nc)
6563.1.1 = { holder=1007008 } # Trebor (nc)
6589.1.1 = { holder=1012008 } # Alleras (nc)
6603.1.1 = { holder=1015008 } # Garyn (nc)
6653.1.1 = { holder=1017008 } # Yandry (nc)
6673.1.1 = { holder=1019008 } # Casyos (nc)
6680.1.1 = { holder=1022008 } # Bors (nc)
6698.1.1 = { holder=1026008 } # Dygos (nc)
6733.1.1 = { holder=1027008 } # Gulyan (nc)
6774.1.1 = { holder=1031008 } # Maron (nc)
6789.1.1 = { holder=1035008 } # Belyar (nc)
6828.1.1 = { holder=1039008 } # Anders (nc)