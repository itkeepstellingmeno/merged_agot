6443.1.1 = {
	holder=100300567 # Daario (nc)
	law = slavery_1
	law = centralization_2
	effect = {
		holder_scope = { 
			e_new_valyria = {
				holder_scope = {
					make_tributary = { who = PREVPREV tributary_type = valyrian_tributary }
				}
			}
		}	
	}
}
6485.1.1 = { holder=100300564 } # Meralyn (nc)
6486.1.1 = { holder=101300564 } # Tagganaro (nc)
6524.1.1 = { holder=101300520 } # Beqqo (nc)
6527.1.1 = { holder=105300520 } # Luco (nc)
6556.1.1 = { holder=105300564 } # Collio (nc)
6560.1.1 = { holder=108300564 } # Tagganaro (nc)
6582.1.1 = { holder=109300520 } # Byan (nc)
6588.1.1 = { holder=113300529 } # Ternesio (nc)
6611.1.1 = { holder=117300520 } # Beqqo (nc)
6631.1.1 = { holder=119300520 } # Moreo (nc)
6642.1.1 = { holder=116300529 } # Vargo (nc)
6652.1.1 = { holder=124300565 } # Ordello (nc)
6663.1.1 = { holder=129300565 } # Moredo (nc)
6696.1.1 = { holder=131300565 } # Tobho (nc)
6702.1.1 = { holder=125300520 } # Joss (nc)
6705.1.1 = { holder=128300520 } # Myrmello (nc)
6708.1.1 = { holder=130300520 } # Brusco (nc)
6719.1.1 = { holder=132300520 } # Collio (nc)
6753.1.1 = { holder=133300564 } # Tagganaro (nc)
6772.1.1 = { holder=134300564 } # Groleo (nc)
