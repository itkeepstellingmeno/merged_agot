1.1.1={
	liege="c_asshai"
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government_city 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}	
}
6468.1.1 = { holder=10032901 } # Sumalika (nc)
6495.1.1 = { holder=10532901 } # Awil (nc)
6521.1.1 = { holder=10732901 } # Dadanum (nc)
6532.1.1 = { holder=11132901 } # Mamagal (nc)
6592.1.1 = { holder=11532901 } # Damuzi (nc)
6611.1.1 = { holder=11932901 } # Qasmath (nc)
6625.1.1 = { holder=12132901 } # Samuqan (nc)
6654.1.1 = { holder=12532901 } # Utu (nc)
6693.1.1 = { holder=12932901 } # Ibnatum (nc)
6718.1.1 = { holder=13332901 } # Qasmath (nc)
6751.1.1 = { holder=13632901 } # Bunuth (nc)
6752.1.1 = { holder=13932901 } # Yatum (nc)


