name = "King Bernarr II's War of Vengeance against Qhored Hoare"

casus_belli={
	casus_belli=bloody_vengeance
	actor=615174432 #Bernarr Justman
	recipient=358066 #Qhored Hoare
	date=7100.1.1
}

7100.1.1 = {
	add_defender = 358066
	add_attacker = 615174432
}

7101.1.1 = {
	rem_defender = 358066
	rem_attacker = 615174432
}                                                                                                                                                                                                                                                                                                                