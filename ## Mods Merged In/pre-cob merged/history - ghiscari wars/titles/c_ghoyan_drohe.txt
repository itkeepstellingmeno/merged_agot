1.1.1={
	law = succ_primogeniture
	law = true_cognatic_succession
	liege = d_velvet_hills
}

2995.1.1 = { holder=1000008 } # Rycas (nc)
3022.1.1 = { holder=1001008 } # Bors (nc)
3051.1.1 = { holder=1003008 } # Caleyen (nc)
3085.1.1 = { holder=1007008 } # Trebor (nc)
3111.1.1 = { holder=1012008 } # Alleras (nc)
3125.1.1 = { holder=1015008 } # Garyn (nc)
3175.1.1 = { holder=1017008 } # Yandry (nc)
3195.1.1 = { holder=1019008 } # Casyos (nc)
3202.1.1 = { holder=1022008 } # Bors (nc)
3220.1.1 = { holder=1026008 } # Dygos (nc)
3255.1.1 = { holder=1027008 } # Gulyan (nc)
3296.1.1 = { holder=1031008 } # Maron (nc)
3311.1.1 = { holder=1035008 } # Belyar (nc)
3350.1.1 = { holder=1039008 } # Anders (nc)
