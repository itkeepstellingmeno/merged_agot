1.1.1={
	law = succ_primogeniture
	law = cognatic_succession
	effect = {
		holder_scope = {
			set_special_character_title = marshking_male
		}	
	}
}
2985.1.1 = { holder=10000906 } # Calon (nc)
3027.1.1 = { holder=10020906 } # Leobald (nc)
3053.1.1 = { holder=10030906 } # Yoren (nc)
3085.1.1 = { holder=10090906 } # Roger (nc)
3134.1.1 = { holder=10100906 } # Domeric (nc)
3160.1.1 = { holder=10130906 } # Marlon (nc)
3188.1.1 = { holder=10140906 } # Alyn (nc)
3205.1.1 = { holder=10180906 } # Helman (nc)
3248.1.1 = { holder=10190906 } # Alyn (nc)
3259.1.1 = { holder=10260906 } # Beron (nc)
3296.1.1 = { holder=10270906 } # Wynton (nc)
3305.1.1 = { holder=10320906 } # Robett (nc)
3328.1.1 = { holder=10350906 } # Larence (nc)

