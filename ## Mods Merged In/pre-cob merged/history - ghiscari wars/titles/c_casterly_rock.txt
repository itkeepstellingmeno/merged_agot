3.1.1={
	liege="d_casterly_rock"
}

40.1.1 = { holder=20174406 } # Corlos, son of Caster # Line of Casterlys
80.1.1 = { holder=21174406 } # Carlon, son of Corlos
120.1.1 = { holder=22174406 } # Caston, son of Carlon
150.1.1 = { holder=23174406 } # Criston, son of Caston
156.1.1 = { holder=24174406 } # Corlos, son of Criston
186.1.1 = { holder=25174406 } # Caster, son of Corlos
198.1.1 = { holder=26174406 } # Castella, daughter of Caster, bride of Lann the Clever

210.1.1 = { holder=80260190 } # Lanna Lannister # Line of Lannisters
251.1.1 = { holder=80270190 } # Loreon the Lion
280.1.1 = { holder=0 }

2995.1.1 = { holder=1000190 } # Daven (nc)
3021.1.1 = { holder=1001190 } # Lorimer (nc)
3059.1.1 = { holder=1002190 } # Tymeon (nc)
3097.1.1 = { holder=1005190 } # Tywin (nc)
3107.1.1 = { holder=1006190 } # Loreon (nc)
3141.1.1 = { holder=1007190 } # Lorimar (nc)
3168.1.1 = { holder=1010190 } # Damon (nc)
3173.1.1 = { holder=1013190 } # Tytos (nc)
3211.1.1 = { holder=1014190 } # Tyland (nc)
3223.1.1 = { holder=1015190 } # Lucas (nc)
3281.1.1 = { holder=1020190 } # Tytos (nc)
3311.1.1 = { holder=1021190 } # Tyryan (nc)

