namespace = braavos

# Braavos announces uncloaking
character_event = { 
	id = braavos.1
	desc = "EVTDESCbraavos.1"
	picture = GFX_braavos
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAbraavos.1"
		e_new_valyria = {
			holder_scope = {
				character_event = { id = braavos.3 days = 1 } #Valyria reacts				
			}	
		}
		hidden_tooltip = {
			any_playable_ruler = {
				limit = {
					NOT = { trait = wildling }
					NOT = { has_landed_title = e_new_valyria }		
				}
				letter_event = { id = braavos.2 days = 1 } #Letters sent to every corner of world
			}	
		}
	}
}

# Braavos letter sent to every corner of the world
letter_event = { 
	id = braavos.2
	desc = "EVTDESCbraavos.2"
	
	is_triggered_only = yes
	
	option = {
		name = OK
	}
}


#  Valyria decides how to react
character_event = {
	id = braavos.3
	desc = "EVTDESCbraavos.3"
	picture = GFX_braavos
	
	hide_from = yes

	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAbraavos.3" # Leave them be
		ai_chance = { factor = 70 }
	
	}
	
	option = {
		name = "EVTOPTBbraavos.3" # Invade them
		ai_chance = { factor = 30 }

		k_braavos = {
			holder_scope = {
				reverse_war = {
					target = ROOT
					casus_belli = slave_raid
				}
			}
		}
		hidden_tooltip = {
			k_braavos = {
				holder_scope = { 
					character_event = { id = braavos.4 days = 1 }
				}
			}	
		}
	}

}

# Braavos informed of Valyrian invasion
character_event = { 
	id = braavos.4
	desc = "EVTDESCbraavos.4"
	picture = GFX_braavos
	
	is_triggered_only = yes
	
	option = {
		name = "EVTOPTAbraavos.4"

	}
}