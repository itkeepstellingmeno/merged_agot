210.1.1 = { 
	holder=150012 #Yoren
	law = succ_primogeniture
	law = true_cognatic_succession
	liege = k_stonewayTK
}
235.1.1 = { holder = 0 }
6400.1.1 = { holder = 152012 } #Yorick I
6440.1.1 = { holder = 153012 } #Yorick II
6468.1.1 = { holder = 154012 } #Olyvar
6481.1.1 = { holder = 0 }

7241.1.1 = { holder = 156012 } #Yorick III
7268.1.1 = { holder = 157012 } #Yorick IV
7295.1.1 = { holder = 158012 } #Yorick V
7311.1.1 = { # after being defeated by the Martell's.
	holder = 0
	liege="e_dorne"
}
7527.1.1 = { holder = 160012 }
7557.1.1 = { holder = 161012 }
7588.1.1 = { holder=30012 } # Yandry (nc)
7607.1.1 = { holder=30212 } # Cletus (nc)
7622.1.1 = { holder=30312 } # Ulrick (nc)
7648.1.1 = { holder=31012 } # Timoth (nc)
7675.1.1 = { holder=31112 } # Anders (nc)
7709.1.1 = { holder=31212 } # Castos (nc)
7710.1.1 = { holder=31312 } # Alaric (nc)
7753.1.1 = { holder=31812 } # Linnet (nc)


7767.1.1 = { holder=32212 } # Ormond (nc)



7801.1.1 = { holder=32312 } # Trystane (nc)
7830.1.1 = { holder=32612 } # Edgar (c)

