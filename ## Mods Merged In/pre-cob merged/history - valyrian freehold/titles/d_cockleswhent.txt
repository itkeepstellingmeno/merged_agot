1.1.1={ law=succ_primogeniture law=cognatic_succession
	liege="e_reach" # 
}

6926.1.1 = { holder=620080 } # Mandel (nc) # Growing power of the Manderlys
6947.1.1 = { holder=630080 } # Mandon (nc)
6960.1.1 = { holder=65900298 } # Lorimar (C) Exiled the Manderlys

6982.1.1 = { holder=0 }

7450.1.1 = { 
	holder=6100298 
	effect = {	
		if = {
			limit = { holder_scope = { has_landed_title = b_starpike } }
			b_starpike = { ROOT = { holder_scope = { capital = PREVPREV } } }
		}
	}
} # Gwayne (nc)
7487.1.1 = { holder=6200298 } # Mervyn (nc)
7495.1.1 = { holder=65000298 } # Perceon (nc)
7530.1.1 = { holder=65100298 } # Galadon (nc)
7567.1.1 = { holder=3000298 } # Mern (nc)
7584.1.1 = { holder=65200298 } # Gareth (nc)
7588.1.1 = { holder=3000298 } # Mern (nc)
7598.6.6 = { holder=3001298 } # Tania (nc)

7617.1.1 = { holder=3004298 } # Mathis (nc)
7665.1.1 = { holder=3008298 } # Morm (nc)
7680.1.1 = { holder=3010298 } # Martyn (nc)
7724.1.1 = { holder=3011298 } # Arryn (nc)
7746.1.1 = { holder=3015298 } # Franklyn (nc)
7770.1.1 = { holder=3019298 } # Horas (nc)
7794.1.1 = { holder=3022298 } # Gormon (nc)

#Given to the Ashfords
7812.1.1={
	holder=88026
}
7820.1.1={
	holder=265
}
7850.1.1={
	holder=88027
}
7876.1.1={
	holder=88028
}




