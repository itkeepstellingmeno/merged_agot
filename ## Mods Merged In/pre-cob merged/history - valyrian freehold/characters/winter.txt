######  White Walkers (six seen in prologue)

999000666 = {
	name="White Walker"
	dynasty=66000666
	religion="cold_gods"
	culture="white_walker"
	
	dna = bafdjbmbaab
	properties = 0j0d0
	
	martial = 0
	diplomacy = 0
	intrigue = 0
	stewardship = 10
	learning = 0

	trait = white_walker
	trait = hunter
	trait = back_seat_leader
	trait = skilled_tactician
	trait = master_warrior
	trait = light_foot_leader
	
	disallow_random_traits = yes
	
	1.1.1 = { 
		birth="1.1.1" 
		effect = {
			set_character_flag = white_walker
		}
	}
}

999001666 = {
	name="White Walker"
	dynasty=66000666
	religion="cold_gods"
	culture="white_walker"
	
	martial = 0
	diplomacy = 0
	intrigue = 0
	stewardship = 10
	learning = 0

	trait = white_walker
	trait = hunter
	trait = back_seat_leader
	trait = skilled_tactician
	trait = skilled_warrior
	trait = light_foot_leader
	
	disallow_random_traits = yes
	
	25.1.1 = { 
		birth=yes
		employer=999000666 
		effect = {
			set_character_flag = white_walker
		}
	}
}

999002666 = {
	name="White Walker"
	dynasty=66000666
	religion="cold_gods"
	culture="white_walker"
	
	martial = 0
	diplomacy = 0
	intrigue = 0
	stewardship = 10
	learning = 0

	trait = white_walker
	trait = hunter
	trait = back_seat_leader
	trait = skilled_tactician
	trait = skilled_warrior
	trait = light_foot_leader
	
	disallow_random_traits = yes
	
	25.1.1 = { 
		birth=yes 
		employer=999000666 
		effect = {
			set_character_flag = white_walker
		}
	}
}

999003666 = {
	name="White Walker"
	dynasty=66000666
	religion="cold_gods"
	culture="white_walker"
	
	martial = 0
	diplomacy = 0
	intrigue = 0
	stewardship = 10
	learning = 0

	trait = white_walker
	trait = hunter
	trait = back_seat_leader
	trait = skilled_tactician
	trait = skilled_warrior
	trait = light_foot_leader
	
	disallow_random_traits = yes
	
	25.1.1 = { 
		birth=yes
		employer=999000666 
		effect = {
			set_character_flag = white_walker
		}
	}
}

999004666 = {
	name="White Walker"
	dynasty=66000666
	religion="cold_gods"
	culture="white_walker"
	
	martial = 0
	diplomacy = 0
	intrigue = 0
	stewardship = 10
	learning = 0

	trait = white_walker
	trait = hunter
	trait = back_seat_leader
	trait = skilled_tactician
	trait = skilled_warrior
	trait = light_foot_leader
	
	disallow_random_traits = yes
	
	25.1.1 = { 
		birth=yes
		employer=999000666 
		effect = {
			set_character_flag = white_walker
		}
	}
}

999005666 = {
	name="White Walker"
	dynasty=66000666
	religion="cold_gods"
	culture="white_walker"
	
	martial = 0
	diplomacy = 0
	intrigue = 0
	stewardship = 10
	learning = 0

	trait = white_walker
	trait = hunter
	trait = back_seat_leader
	trait = skilled_tactician
	trait = skilled_warrior
	trait = light_foot_leader
	
	disallow_random_traits = yes
	
	25.1.1 = { 
		birth=yes
		employer=999000666 
		effect = {
			set_character_flag = white_walker
		}

	}
}

#White Walker of the East
999006666 = {
	name="White Walker"
	dynasty=66000666
	religion="cold_gods"
	culture="white_walker"
	
	martial = 0
	diplomacy = 0
	intrigue = 0
	stewardship = 10
	learning = 0

	trait = white_walker
	trait = hunter
	trait = back_seat_leader
	trait = skilled_tactician
	trait = master_warrior
	trait = light_foot_leader
	
	disallow_random_traits = yes
	
	7500.1.1 = { 
		birth="7500.1.1" 
		effect = {
			set_character_flag = white_walker
		}
	}
}
