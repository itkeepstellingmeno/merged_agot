1.1.1={ law=succ_primogeniture law=cognatic_succession
	liege="d_oldtown"
}
25.1.1={holder=6000285} # Uthor of the High tower (C)
94.1.1={holder=6100285} # Urrigon (C)
120.1.1={holder=6200285} # Otho I (C)
140.1.1={holder=6300285} # Uthor II
175.1.1={holder=6400285} # Otho II (C)
195.1.1={holder=0}
2670.1.1={holder=6700285} # Lymond the Sea Lion # Last king of the Oldtown (C)
2723.1.1={holder=0}

6350.1.1={holder=6900285} # Jeremy (C)
6370.1.1={holder=61000285} # Jason (C)
6390.1.1={holder=61100285} # Jared
6445.1.1={holder=61200285} # Jarett
6465.1.1={holder=61300285} # Lymond II
6515.1.1={holder=61400285} # John
6555.1.1={holder=61500285} # Otto
6560.1.1={holder=61600285} # Gareth
6580.1.1={holder=61700285} # Dorian (C)
6638.1.1={holder=61800285} # Ormund
6650.1.1={holder=61900285} # Damon the Devout (C) # First to convert to the seven
6665.1.1={holder=62000285} # Triston (C)
6715.1.1={holder=62100285} # Barris (C)
6760.1.1={holder=0}
6950.1.1={holder=62300285} # Orton
6970.1.1={holder=63200285} # Runcel
6990.1.1={holder=62400285} # Lymond III
7040.1.1={holder=62600285} # Oscar
7057.1.1={holder=63100285} # Morgan

7060.1.1={
	holder= 21285 # Banfred Hightower
}
7094.1.1={
	holder= 17285 # Manfred Hightower -  He opened his gates to Aegon the Conqueror at the High Septon's urging.
}
7120.1.1={
	holder= 16285 # Martyn Hightower 	
}
7142.1.1={
	holder= 177285 # Ben Hightower 
}
7180.1.1={
	holder= 14285 # Runcel Hightower 
}
7205.1.1={
	holder= 11285 # Lyonel Hightower 
}
7220.1.1={
	holder= 12285 # Ormund Hightower 
}
7230.8.1={
	holder= 175285 # Manfred Hightower 
}
7275.1.1={
	holder= 10285 # Jon Hightower 
}
7315.1.1={
	holder= 9285 # Abelar Hightower 
}
7326.1.1={
	holder= 88017 # Quenton Hightower - Died in 8232
}

