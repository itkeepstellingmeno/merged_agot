6506.1.1={
	liege="e_north"
	law = succ_appointment
	effect = {
		set_title_flag = military_command
		holder_scope = {
			if = {
				limit = { primary_title = { title = PREVPREV } }
				set_government_type = military_command_government 
				PREV = { succession = appointment }
				recalc_succession = yes
			}
		}
	}	
}

7095.1.1 = { holder=100059 } # Brandon (nc)
7134.1.1 = { holder=100159 } # Eddard (nc)
7153.1.1 = { holder=100259 } # Rodwell (nc)
7175.1.1 = { holder=100359 } # Rickard (nc)
7193.1.1 = { holder=100459 } # Donnor (nc)
7232.1.1= {
    holder = 1120159 # Brandon the Daughterless
}
7267.12.12= {
    holder = 1120459 # Jon, Bael's son
}
7298.9.9= {
    holder = 8311059 # Harlon
}
7324.9.9= {
    holder = 81321059 # Ellard
}
7352.1.1= {
    holder = 8331059 # Barth
}

