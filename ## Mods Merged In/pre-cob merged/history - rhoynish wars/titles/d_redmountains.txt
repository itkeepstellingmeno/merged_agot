4000.1.1={
	liege="k_torrentineTK"
	law = succ_primogeniture
	law = cognatic_succession
	effect = { #cull spawned western valyrians
		holder_scope = {
			any_realm_character = {
				limit = {
					culture_group = valyrian
					NOT = { father_even_if_dead = { always = yes } }
					NOT = { mother_even_if_dead = { always = yes } }
					OR = {
						dynasty = 0
						tier = BARON
					}
					NOT = { has_character_flag = high_valyrian }
					NOT = { has_dynasty_flag = high_valyrian }
					high_valyrian_dynasty_trigger = no
				}
				culture = dornish_andal
				set_graphical_culture = dornish_andal
			}
		}
	}
}
4751.1.1 = {
	holder = 150016 #Samwell
}
4798.1.1 = {
	holder = 0
}

7035.1.1 = { holder=60016 } # Arthur (nc)
7077.1.1 = { holder=24017 } # Gerold (nc)
7101.2.5= {
holder=23017
}
7154.1.1= {
holder=22017
}
7176.1.1= {
holder=21017
}
7198.1.1= {
holder=20017
}
7212.1.1= {
holder=18017
}
7232.1.1= {
holder=17017
}

7258.8.4= {
	holder=15017
}
7259.3.5= {
holder=13017
}

7294.1.1 = {
	holder = 152016 #Vorian
}
7305.1.1 = {
	holder = 153016 #Trystane
	liege="e_dorne"
}
7355.1.1 = {
	holder = 155016 #Maron
}
