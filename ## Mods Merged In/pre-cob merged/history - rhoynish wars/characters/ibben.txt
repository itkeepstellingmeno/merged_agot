####  House Moth

1055927 = {
	name="Togg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	7055.1.1 = {birth="7055.1.1"}
	7071.1.1 = {add_trait=poor_warrior}
	7073.1.1 = {add_spouse=4055927}
	7122.1.1 = {death="7122.1.1"}
}
4055927 = {
	name="Cossoma"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7055.1.1 = {birth="7055.1.1"}
	7122.1.1 = {death="7122.1.1"}
}
1155927 = {
	name="Togg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7084.1.1 = {birth="7084.1.1"}
	7100.1.1 = {add_trait=trained_warrior}
	7102.1.1 = {add_spouse=41055927}
	7140.1.1 = {death="7140.1.1"}
}
41055927 = {
	name="Cossoma"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7084.1.1 = {birth="7084.1.1"}
	7140.1.1 = {death="7140.1.1"}
}
1255927 = {
	name="Torma"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7087.1.1 = {birth="7087.1.1"}
	7118.1.1 = {death="7118.1.1"}
}
1355927 = {
	name="Brea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7089.1.1 = {birth="7089.1.1"}
	7145.1.1 = {death="7145.1.1"}
}
1455927 = {
	name="Yorka"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1055927
	mother=4055927

	7091.1.1 = {birth="7091.1.1"}
	7146.1.1 = {death="7146.1.1"}
}
1555927 = {
	name="Tagganaria"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1155927
	mother=41055927

	7112.1.1 = {birth="7112.1.1"}
	7160.1.1 = {death="7160.1.1"}
}
1655927 = {
	name="Groo"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1155927
	mother=41055927

	7114.1.1 = {birth="7114.1.1"}
	7130.1.1 = {add_trait=poor_warrior}
	7132.1.1 = {add_spouse=4655927}
	7142.1.1 = {death="7142.1.1"}
}
4655927 = {
	name="Mera"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7114.1.1 = {birth="7114.1.1"}
	7142.1.1 = {death="7142.1.1"}
}
1755927 = {
	name="Caloth"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1655927
	mother=4655927

	7141.1.1 = {birth="7141.1.1"}
	7157.1.1 = {add_trait=trained_warrior}
	7159.1.1 = {add_spouse=4755927}
	7181.1.1 = {death="7181.1.1"}
}
4755927 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7141.1.1 = {birth="7141.1.1"}
	7181.1.1 = {death="7181.1.1"}
}
1855927 = {
	name="Mordea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1755927
	mother=4755927

	7170.1.1 = {birth="7170.1.1"}
	7231.1.1 = {death="7231.1.1"}
}
1955927 = {
	name="Tugg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1755927
	mother=4755927

	7173.1.1 = {birth="7173.1.1"}
	7189.1.1 = {add_trait=trained_warrior}
	7191.1.1 = {add_spouse=4955927}
	7227.1.1 = {death="7227.1.1"}
}
4955927 = {
	name="Rowan"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7173.1.1 = {birth="7173.1.1"}
	7227.1.1 = {death="7227.1.1"}
}
2055927 = {
	name="Jaqea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1955927
	mother=4955927

	7198.1.1 = {birth="7198.1.1"}
	7253.1.1 = {death="7253.1.1"}
}
2155927 = {
	name="Galt"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=1955927
	mother=4955927

	7200.1.1 = {birth="7200.1.1"}
	7216.1.1 = {add_trait=poor_warrior}
	7218.1.1 = {add_spouse=4155927}
	7246.1.1 = {death="7246.1.1"}
}
4155927 = {
	name="Yna"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7200.1.1 = {birth="7200.1.1"}
	7246.1.1 = {death="7246.1.1"}
}
2255927 = {
	name="Galt"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2155927
	mother=4155927

	7227.1.1 = {birth="7227.1.1"}
	7243.1.1 = {add_trait=skilled_warrior}
	7245.1.1 = {add_spouse=42055927}
	7259.1.1 = {death="7259.1.1"}
}
42055927 = {
	name="Betharios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7227.1.1 = {birth="7227.1.1"}
	7299.1.1 = {death="7299.1.1"}
}
2355927 = {
	name="Joth"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2155927
	mother=4155927

	7230.1.1 = {birth="7230.1.1"}
	7246.1.1 = {add_trait=skilled_warrior}
	7273.1.1 = {death="7273.1.1"}
}
2455927 = {
	name="Ulf"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2155927
	mother=4155927

	7232.1.1 = {birth="7232.1.1"}
	7248.1.1 = {add_trait=poor_warrior}
	7268.1.1 = {death="7268.1.1"}
}
2555927 = {
	name="Slugg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2255927
	mother=42055927

	7257.1.1 = {birth="7257.1.1"}
	7273.1.1 = {add_trait=skilled_warrior}
	7280.1.1 = {add_spouse=4555927}
	7329.1.1 = {death="7329.1.1"}
}
4555927 = {
	name="Jaqea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7262.1.1 = {birth="7262.1.1"}
	7329.1.1 = {death="7329.1.1"}
}
2655927 = {
	name="Zia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2255927
	mother=42055927

	7258.1.1 = {birth="7258.1.1"}
	7342.1.1 = {death="7342.1.1"}
}
2755927 = {
	name="Denia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2555927
	mother=4555927

	7294.1.1 = {birth="7294.1.1"}
	7357.1.1 = {death="7357.1.1"}
}
2855927 = {
	name="Togg"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2555927
	mother=4555927

	7295.1.1 = {birth="7295.1.1"}
	7311.1.1 = {add_trait=trained_warrior}
	7313.1.1 = {add_spouse=4855927}
	7355.1.1 = {death="7355.1.1"}
}
4855927 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7295.1.1 = {birth="7295.1.1"}
	7355.1.1 = {death="7355.1.1"}
}
2955927 = {
	name="Zia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2555927
	mother=4555927

	7297.1.1 = {birth="7297.1.1"}
	7354.1.1 = {death="7354.1.1"}
}
3055927 = {
	name="Denia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2855927
	mother=4855927

	7319.1.1 = {birth="7319.1.1"}
	7351.1.1 = {death="7351.1.1"}
}
3155927 = {
	name="Gylenios"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2855927
	mother=4855927

	7320.1.1 = {birth="7320.1.1"}
	7353.1.1 = {death="7353.1.1"}
}
3255927 = {
	name="Krull"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=2855927
	mother=4855927

	7323.1.1 = {birth="7323.1.1"}
	7339.1.1 = {add_trait=poor_warrior}
	7341.1.1 = {add_spouse=4255927}
	7363.1.1 = {death="7363.1.1"}
}
4255927 = {
	name="Mellario"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7323.1.1 = {birth="7323.1.1"}
	7363.1.1 = {death="7363.1.1"}
}
3355927 = {
	name="Durr"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3255927
	mother=4255927

	7341.1.1 = {birth="7341.1.1"}
	7357.1.1 = {add_trait=poor_warrior}
	7359.1.1 = {add_spouse=4355927}
	7396.1.1 = {death="7396.1.1"}
}
4355927 = {
	name="Mordea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7341.1.1 = {birth="7341.1.1"}
	7396.1.1 = {death="7396.1.1"}
}
3455927 = {
	name="Mordea"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3255927
	mother=4255927

	7343.1.1 = {birth="7343.1.1"}
	7389.1.1 = {death="7389.1.1"}
}
3555927 = {
	name="Togg"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3255927
	mother=4255927

	7346.1.1 = {birth="7346.1.1"}
	7362.1.1 = {add_trait=trained_warrior}
}
3655927 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7359.1.1 = {birth="7359.1.1"}
	7391.1.1 = {death="7391.1.1"}
}
3755927 = {
	name="Holger"	# a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7360.1.1 = {birth="7360.1.1"}
	7376.1.1 = {add_trait=skilled_warrior}
}
3855927 = {
	name="Meralyia"	# not a lord
	female=yes
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7363.1.1 = {birth="7363.1.1"}
	7390.1.1 = {death="7390.1.1"}
}
3955927 = {
	name="Jogg"	# not a lord
	dynasty=55927

	religion="gods_ibben"
	culture="ibbenese"

	father=3355927
	mother=4355927

	7366.1.1 = {birth="7366.1.1"}
	7382.1.1 = {add_trait=trained_warrior}
}


####  House Bartogg

1055928 = {
	name="Krull"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	7068.1.1 = {birth="7068.1.1"}
	7084.1.1 = {add_trait=poor_warrior}
	7086.1.1 = {add_spouse=40055928}
	7145.1.1 = {death="7145.1.1"}
}
40055928 = {
	name="Denia"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7068.1.1 = {birth="7068.1.1"}
	7145.1.1 = {death="7145.1.1"}
}
1155928 = {
	name="Haggon"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1055928
	mother=40055928

	7095.1.1 = {birth="7095.1.1"}
	7111.1.1 = {add_trait=trained_warrior}
	7113.1.1 = {add_spouse=41055928}
	7153.1.1 = {death="7153.1.1"}
}
41055928 = {
	name="Mera"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7095.1.1 = {birth="7095.1.1"}
	7153.1.1 = {death="7153.1.1"}
}
1255928 = {
	name="Jarr"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1155928
	mother=41055928

	7122.1.1 = {birth="7122.1.1"}
	7138.1.1 = {add_trait=poor_warrior}
	7140.1.1 = {add_spouse=42055928}
	7157.1.1 = {death="7157.1.1"}
}
42055928 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7122.1.1 = {birth="7122.1.1"}
	7157.1.1 = {death="7157.1.1"}
}
1355928 = {
	name="Torma"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1155928
	mother=41055928

	7124.1.1 = {birth="7124.1.1"}
	7169.1.1 = {death="7169.1.1"}
}
1455928 = {
	name="Umar"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1255928
	mother=42055928

	7153.1.1 = {birth="7153.1.1"}
	7169.1.1 = {add_trait=poor_warrior}
	7171.1.1 = {add_spouse=44055928}
	7192.1.1 = {death="7192.1.1"}
}
44055928 = {
	name="Gylenios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7153.1.1 = {birth="7153.1.1"}
	7192.1.1 = {death="7192.1.1"}
}
1555928 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1255928
	mother=42055928

	7155.1.1 = {birth="7155.1.1"}
	7211.1.1 = {death="7211.1.1"}
}
1655928 = {
	name="Assadora"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1255928
	mother=42055928

	7156.1.1 = {birth="7156.1.1"}
	7211.1.1 = {death="7211.1.1"}
}
1755928 = {
	name="Krull"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7176.1.1 = {birth="7176.1.1"}
	7192.1.1 = {add_trait=trained_warrior}
	7194.1.1 = {add_spouse=4755928}
	7212.1.1 = {death="7212.1.1"}
}
4755928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7176.1.1 = {birth="7176.1.1"}
	7212.1.1 = {death="7212.1.1"}
}
1855928 = {
	name="Clargg"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7178.1.1 = {birth="7178.1.1"}
	7194.1.1 = {add_trait=poor_warrior}
	7247.1.1 = {death="7247.1.1"}
}
1955928 = {
	name="Bellegere"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7180.1.1 = {birth="7180.1.1"}
	7211.1.1 = {death="7211.1.1"}
}
2055928 = {
	name="Ynys"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7181.1.1 = {birth="7181.1.1"}
	7237.1.1 = {death="7237.1.1"}
}
2155928 = {
	name="Erroth"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1455928
	mother=44055928

	7182.1.1 = {birth="7182.1.1"}
	7198.1.1 = {add_trait=trained_warrior}
	7239.1.1 = {death="7239.1.1"}
}
2255928 = {
	name="Slugg"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=1755928
	mother=4755928

	7197.1.1 = {birth="7197.1.1"}
	7213.1.1 = {add_trait=skilled_warrior}
	7215.1.1 = {add_spouse=4255928}
	7260.1.1 = {death="7260.1.1"}
}
4255928 = {
	name="Yorka"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7197.1.1 = {birth="7197.1.1"}
	7260.1.1 = {death="7260.1.1"}
}
2355928 = {
	name="Torma"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7222.1.1 = {birth="7222.1.1"}
	7273.1.1 = {death="7273.1.1"}
}
2455928 = {
	name="Izembara"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7225.1.1 = {birth="7225.1.1"}
	7261.1.1 = {death="7261.1.1"}
}
2555928 = {
	name="Sogg"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7228.1.1 = {birth="7228.1.1"}
	7244.1.1 = {add_trait=poor_warrior}
	7246.1.1 = {add_spouse=4555928}
	7265.1.1 = {death="7265.1.1"}
}
4555928 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7228.1.1 = {birth="7228.1.1"}
	7265.1.1 = {death="7265.1.1"}
}
2655928 = {
	name="Meralyia"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7231.1.1 = {birth="7231.1.1"}
	7300.1.1 = {death = {death_reason = death_murder}}
}
2755928 = {
	name="Brella"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2255928
	mother=4255928

	7232.1.1 = {birth="7232.1.1"}
	7280.1.1 = {death="7280.1.1"}
}
2855928 = {
	name="Joth"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7241.1.1 = {birth="7241.1.1"}
	7257.1.1 = {add_trait=trained_warrior}
	7258.1.1 = {add_spouse=48055928}
	7297.1.1 = {death = {death_reason = death_battle}}
}
48055928 = {
	name="Izembara"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7241.1.1 = {birth="7241.1.1"}
	7297.1.1 = {death="7297.1.1"}
}
2955928 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7254.1.1 = {birth="7254.1.1"}
	7310.1.1 = {death="7310.1.1"}
}
3055928 = {
	name="Meralyia"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7255.1.1 = {birth="7255.1.1"}
	7294.1.1 = {death="7294.1.1"}
}
3155928 = {
	name="Galt"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2555928
	mother=4555928

	7256.1.1 = {birth="7256.1.1"}
	7272.1.1 = {add_trait=poor_warrior}
	7306.1.1 = {death="7306.1.1"}
}
3255928 = {
	name="Rowan"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2855928
	mother=48055928

	7259.1.1 = {birth="7259.1.1"}
	7345.1.1 = {death="7345.1.1"}
}
3355928 = {
	name="Morr"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=2855928
	mother=48055928

	7260.1.1 = {birth="7260.1.1"}
	7309.1.1 = {add_trait=poor_warrior}
	7311.1.1 = {add_spouse=4355928}
	7347.1.1 = {death="7347.1.1"}
}
4355928 = {
	name="Yna"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7260.1.1 = {birth="7260.1.1"}
	7347.1.1 = {death="7347.1.1"}
}
3455928 = {
	name="Colosh"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3355928
	mother=4355928

	7317.1.1 = {birth="7317.1.1"}
	7333.1.1 = {add_trait=poor_warrior}
	7335.1.1 = {add_spouse=4455928}
	7374.1.1 = {death="7374.1.1"}
}
4455928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7317.1.1 = {birth="7317.1.1"}
	7374.1.1 = {death="7374.1.1"}
}
3555928 = {
	name="Jarr"	# not a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3355928
	mother=4355928

	7319.1.1 = {birth="7319.1.1"}
	7335.1.1 = {add_trait=poor_warrior}
	7365.1.1 = {death="7365.1.1"}
}
3655928 = {
	name="Ynys"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3355928
	mother=4355928

	7322.1.1 = {birth="7322.1.1"}
	7343.1.1 = {death="7343.1.1"}
}
3755928 = {
	name="Izembara"	# not a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3455928
	mother=4455928

	7340.1.1 = {birth="7340.1.1"}
}
3855928 = {
	name="Drogg"	# a lord
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3455928
	mother=4455928

	7341.1.1 = {birth="7341.1.1"}
	7357.1.1 = {add_trait=skilled_warrior}
	7359.1.1 = {add_spouse=4855928}
	7383.1.1 = {death="7383.1.1"}
}
4855928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7341.1.1 = {birth="7341.1.1"}
	7383.1.1 = {death="7383.1.1"}
}
3955928 = {
	name="Betharios"	# a lord
	female=yes
	dynasty=55928

	religion="gods_ibben"
	culture="ibbenese"

	father=3855928
	mother=4855928

	7376.1.1 = {birth="7376.1.1"}
}
4055928 = {
	name="Harra"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855928
	mother=4855928

	7377.1.1 = {birth="7377.1.1"}
}
4155928 = {
	name="Yorka"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855928
	mother=4855928

	7380.1.1 = {birth="7380.1.1"}
}


####  House Joth

1055929 = {
	name="Slugg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	7069.1.1 = {birth="7069.1.1"}
	7085.1.1 = {add_trait=poor_warrior}
	7087.1.1 = {add_spouse=46755929}
	7136.1.1 = {death="7136.1.1"}
}
46755929 = {
	name="Assadora"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7069.1.1 = {birth="7069.1.1"}
	7136.1.1 = {death="7136.1.1"}
}
1155929 = {
	name="Gylenios"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7091.1.1 = {birth="7091.1.1"}
	7101.1.1 = {death = {death_reason = death_murder}}
}
1255929 = {
	name="Bodger"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7094.1.1 = {birth="7094.1.1"}
	7110.1.1 = {add_trait=poor_warrior}
	7112.1.1 = {add_spouse=4255929}
	7148.1.1 = {death="7148.1.1"}
}
4255929 = {
	name="Brea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7094.1.1 = {birth="7094.1.1"}
	7148.1.1 = {death="7148.1.1"}
}
1355929 = {
	name="Sogoloth"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7096.1.1 = {birth="7096.1.1"}
	7098.1.1 = {death="7098.1.1"}
}
1455929 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1055929
	mother=46755929

	7099.1.1 = {birth="7099.1.1"}
	7137.1.1 = {death="7137.1.1"}
}
1555929 = {
	name="Harra"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1255929
	mother=4255929

	7113.1.1 = {birth="7113.1.1"}
	7184.1.1 = {death = {death_reason = death_murder}}
}
1655929 = {
	name="Tugg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1255929
	mother=4255929

	7115.1.1 = {birth="7115.1.1"}
	7131.1.1 = {add_trait=skilled_warrior}
	7133.1.1 = {add_spouse=4655929}
	7185.1.1 = {death="7185.1.1"}
}
4655929 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7115.1.1 = {birth="7115.1.1"}
	7185.1.1 = {death="7185.1.1"}
}
1755929 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1655929
	mother=4655929

	7137.1.1 = {birth="7137.1.1"}
	7179.1.1 = {death="7179.1.1"}
}
1855929 = {
	name="Jogg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1655929
	mother=4655929

	7138.1.1 = {birth="7138.1.1"}
	7154.1.1 = {add_trait=poor_warrior}
	7156.1.1 = {add_spouse=48055929}
	7189.1.1 = {death="7189.1.1"}
}
48055929 = {
	name="Mellario"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7138.1.1 = {birth="7138.1.1"}
	7189.1.1 = {death="7189.1.1"}
}
1955929 = {
	name="Joth"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1655929
	mother=4655929

	7140.1.1 = {birth="7140.1.1"}
	7156.1.1 = {add_trait=poor_warrior}
	7185.1.1 = {death="7185.1.1"}
}
2055929 = {
	name="Bodger"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7165.1.1 = {birth="7165.1.1"}
	7181.1.1 = {add_trait=poor_warrior}
	7183.1.1 = {add_spouse=40155929}
	7221.1.1 = {death="7221.1.1"}
}
40155929 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7165.1.1 = {birth="7165.1.1"}
	7221.1.1 = {death="7221.1.1"}
}
2155929 = {
	name="Holger"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7168.1.1 = {birth="7168.1.1"}
	7184.1.1 = {add_trait=poor_warrior}
	7214.1.1 = {death="7214.1.1"}
}
2255929 = {
	name="Krull"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7171.1.1 = {birth="7171.1.1"}
	7187.1.1 = {add_trait=poor_warrior}
	7213.1.1 = {death="7213.1.1"}
}
2355929 = {
	name="Jogg"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=1855929
	mother=48055929

	7174.1.1 = {birth="7174.1.1"}
	7190.1.1 = {add_trait=poor_warrior}
	7246.1.1 = {death="7246.1.1"}
}
2455929 = {
	name="Quort"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7188.1.1 = {birth="7188.1.1"}
	7204.1.1 = {add_trait=poor_warrior}
	7206.1.1 = {add_spouse=44055929}
	7249.1.1 = {death="7249.1.1"}
}
44055929 = {
	name="Gylenios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7188.1.1 = {birth="7188.1.1"}
	7249.1.1 = {death="7249.1.1"}
}
2555929 = {
	name="Togg"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7189.1.1 = {birth="7189.1.1"}
	7205.1.1 = {add_trait=skilled_warrior}
	7221.1.1 = {death="7221.1.1"}
}
2655929 = {
	name="Mellario"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7190.1.1 = {birth="7190.1.1"}
	7247.1.1 = {death="7247.1.1"}
}
2755929 = {
	name="Bellegere"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2055929
	mother=40155929

	7191.1.1 = {birth="7191.1.1"}
	7217.1.1 = {death="7217.1.1"}
}
2855929 = {
	name="Denia"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7220.1.1 = {birth="7220.1.1"}
	7290.1.1 = {death="7290.1.1"}
}
2955929 = {
	name="Draloth"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7221.1.1 = {birth="7221.1.1"}
	7237.1.1 = {add_trait=poor_warrior}
	7239.1.1 = {add_spouse=49055929}
	7262.1.1 = {death="7262.1.1"}
}
49055929 = {
	name="Brella"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7221.1.1 = {birth="7221.1.1"}
	7262.1.1 = {death="7262.1.1"}
}
3055929 = {
	name="Ulf"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7224.1.1 = {birth="7224.1.1"}
	7240.1.1 = {add_trait=skilled_warrior}
	7254.1.1 = {death="7254.1.1"}
}
3155929 = {
	name="Holger"	# not a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2455929
	mother=44055929

	7225.1.1 = {birth="7225.1.1"}
	7241.1.1 = {add_trait=skilled_warrior}
	7279.1.1 = {death="7279.1.1"}
}
3255929 = {
	name="Lanna"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2955929
	mother=49055929

	7249.1.1 = {birth="7249.1.1"}
	7304.1.1 = {death="7304.1.1"}
}
3355929 = {
	name="Ygon"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=2955929
	mother=49055929

	7252.1.1 = {birth="7252.1.1"}
	7268.1.1 = {add_trait=poor_warrior}
	7270.1.1 = {add_spouse=4355929}
	7306.1.1 = {death="7306.1.1"}
}
4355929 = {
	name="Yorka"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7252.1.1 = {birth="7252.1.1"}
	7306.1.1 = {death="7306.1.1"}
}
3455929 = {
	name="Cull"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3355929
	mother=4355929

	7273.1.1 = {birth="7273.1.1"}
	7289.1.1 = {add_trait=poor_warrior}
	7291.1.1 = {add_spouse=4455929}
	7314.1.1 = {death="7314.1.1"}
}
4455929 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7273.1.1 = {birth="7273.1.1"}
	7314.1.1 = {death="7314.1.1"}
}
3555929 = {
	name="Harra"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3355929
	mother=4355929

	7276.1.1 = {birth="7276.1.1"}
	7322.1.1 = {death = {death_reason = death_accident}}
}
3655929 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3455929
	mother=4455929

	7298.1.1 = {birth="7298.1.1"}
	7358.1.1 = {death="7358.1.1"}
}
3755929 = {
	name="Betharios"	# not a lord
	female=yes
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3455929
	mother=4455929

	7301.1.1 = {birth="7301.1.1"}
	7342.1.1 = {death="7342.1.1"}
}
3855929 = {
	name="Brogg"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3455929
	mother=4455929

	7303.1.1 = {birth="7303.1.1"}
	7319.1.1 = {add_trait=poor_warrior}
	7321.1.1 = {add_spouse=4855929}
	7358.1.1 = {death="7358.1.1"}
}
4855929 = {
	name="Ferny"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7303.1.1 = {birth="7303.1.1"}
	7358.1.1 = {death="7358.1.1"}
}
3955929 = {
	name="Groo"	# a lord
	dynasty=55929

	religion="gods_ibben"
	culture="ibbenese"

	father=3855929
	mother=4855929

	7338.1.1 = {birth="7338.1.1"}
	7354.1.1 = {add_trait=poor_warrior}
	7356.1.1 = {add_spouse=4955929}
	7397.1.1 = {death="7397.1.1"}
}
4955929 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7338.1.1 = {birth="7338.1.1"}
	7397.1.1 = {death="7397.1.1"}
}
4055929 = {
	name="Rogga"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855929
	mother=4855929

	7339.1.1 = {birth="7339.1.1"}
	7374.1.1 = {death="7374.1.1"}
}
40055929 = {
	name="Slugg"

	religion="gods_ibben"
	culture="ibbenese"

	father=3955929
	mother=4955929

	7366.1.1 = {birth="7366.1.1"}
	7382.1.1 = {add_trait=poor_warrior}
}
4155929 = {
	name="Assadora"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3955929
	mother=4955929

	7369.1.1 = {birth="7369.1.1"}
}


####  House Sluggolsh

1055931 = {
	name="Cull"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	7062.1.1 = {birth="7062.1.1"}
	7078.1.1 = {add_trait=poor_warrior}
	7080.1.1 = {add_spouse=40155931}
	7129.1.1 = {death="7129.1.1"}
}
40155931 = {
	name="Tagganaria"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7062.1.1 = {birth="7062.1.1"}
	7129.1.1 = {death="7129.1.1"}
}
1155931 = {
	name="Jaqea"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7086.1.1 = {birth="7086.1.1"}
	7113.1.1 = {death="7113.1.1"}
}
1255931 = {
	name="Gurn"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7089.1.1 = {birth="7089.1.1"}
	7105.1.1 = {add_trait=poor_warrior}
	7107.1.1 = {add_spouse=42255931}
	7141.1.1 = {death="7141.1.1"}
}
42255931 = {
	name="Gylenios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7089.1.1 = {birth="7089.1.1"}
	7141.1.1 = {death="7141.1.1"}
}
1355931 = {
	name="Krull"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7090.1.1 = {birth="7090.1.1"}
	7106.1.1 = {add_trait=trained_warrior}
	7170.1.1 = {death="7170.1.1"}
}
1455931 = {
	name="Ferny"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7091.1.1 = {birth="7091.1.1"}
	7135.1.1 = {death="7135.1.1"}
}
1555931 = {
	name="Erroth"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1055931
	mother=40155931

	7094.1.1 = {birth="7094.1.1"}
	7110.1.1 = {add_trait=trained_warrior}
	7144.1.1 = {death="7144.1.1"}
}
1655931 = {
	name="Frynne"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1255931
	mother=42255931

	7110.1.1 = {birth="7110.1.1"}
	7161.1.1 = {death="7161.1.1"}
}
1755931 = {
	name="Jogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1255931
	mother=42255931

	7112.1.1 = {birth="7112.1.1"}
	7128.1.1 = {add_trait=trained_warrior}
	7130.1.1 = {add_spouse=47055931}
	7149.1.1 = {death = {death_reason = death_murder}}
}
47055931 = {
	name="Izembara"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7112.1.1 = {birth="7112.1.1"}
	7149.1.1 = {death="7149.1.1"}
}
1855931 = {
	name="Grisel"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7132.1.1 = {birth="7132.1.1"}
	7153.1.1 = {death="7153.1.1"}
}
1955931 = {
	name="Brogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7133.1.1 = {birth="7133.1.1"}
	7149.1.1 = {add_trait=trained_warrior}
	7151.1.1 = {add_spouse=49055931}
	7206.1.1 = {death="7206.1.1"}
}
49055931 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7133.1.1 = {birth="7133.1.1"}
	7206.1.1 = {death="7206.1.1"}
}
2055931 = {
	name="Quort"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7136.1.1 = {birth="7136.1.1"}
	7152.1.1 = {add_trait=skilled_warrior}
	7197.1.1 = {death="7197.1.1"}
}
2155931 = {
	name="Rowan"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1755931
	mother=47055931

	7138.1.1 = {birth="7138.1.1"}
	7217.1.1 = {death="7217.1.1"}
}
2255931 = {
	name="Jogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7165.1.1 = {birth="7165.1.1"}
	7181.1.1 = {add_trait=trained_warrior}
	7183.1.1 = {add_spouse=42155931}
	7232.1.1 = {death="7232.1.1"}
}
42155931 = {
	name="Mordea"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7165.1.1 = {birth="7165.1.1"}
	7232.1.1 = {death="7232.1.1"}
}
2355931 = {
	name="Cossoma"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7166.1.1 = {birth="7166.1.1"}
	7200.1.1 = {death="7200.1.1"}
}
2455931 = {
	name="Gylenios"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7169.1.1 = {birth="7169.1.1"}
	7221.1.1 = {death="7221.1.1"}
}
2555931 = {
	name="Assadora"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=1955931
	mother=49055931

	7172.1.1 = {birth="7172.1.1"}
	7226.1.1 = {death="7226.1.1"}
}
2655931 = {
	name="Durr"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7188.1.1 = {birth="7188.1.1"}
	7204.1.1 = {add_trait=trained_warrior}
	7206.1.1 = {add_spouse=4655931}
	7258.1.1 = {death="7258.1.1"}
}
4655931 = {
	name="Yna"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7188.1.1 = {birth="7188.1.1"}
	7258.1.1 = {death="7258.1.1"}
}
2755931 = {
	name="Slugg"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7190.1.1 = {birth="7190.1.1"}
	7206.1.1 = {add_trait=trained_warrior}
	7249.1.1 = {death="7249.1.1"}
}
2855931 = {
	name="Jarr"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7193.1.1 = {birth="7193.1.1"}
	7209.1.1 = {add_trait=poor_warrior}
	7269.1.1 = {death="7269.1.1"}
}
2955931 = {
	name="Ynys"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7194.1.1 = {birth="7194.1.1"}
	7236.1.1 = {death="7236.1.1"}
}
3055931 = {
	name="Haggon"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2255931
	mother=42155931

	7195.1.1 = {birth="7195.1.1"}
	7211.1.1 = {add_trait=poor_warrior}
	7257.1.1 = {death="7257.1.1"}
}
3155931 = {
	name="Umma"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2655931
	mother=4655931

	7211.1.1 = {birth="7211.1.1"}
	7246.1.1 = {death="7246.1.1"}
}
3255931 = {
	name="Howd"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2655931
	mother=4655931

	7212.1.1 = {birth="7212.1.1"}
	7228.1.1 = {add_trait=trained_warrior}
	7255.1.1 = {death="7255.1.1"}
}
3355931 = {
	name="Bodger"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=2655931
	mother=4655931

	7215.1.1 = {birth="7215.1.1"}
	7231.1.1 = {add_trait=poor_warrior}
	7233.1.1 = {add_spouse=4355931}
	7269.1.1 = {death="7269.1.1"}
}
4355931 = {
	name="Tagganaria"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7215.1.1 = {birth="7215.1.1"}
	7269.1.1 = {death="7269.1.1"}
}
3455931 = {
	name="Izembara"	# not a lord
	female=yes
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3355931
	mother=4355931

	7242.1.1 = {birth="7242.1.1"}
	7309.1.1 = {death="7309.1.1"}
}
3555931 = {
	name="Cull"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3355931
	mother=4355931

	7244.1.1 = {birth="7244.1.1"}
	7260.1.1 = {add_trait=poor_warrior}
	7262.1.1 = {add_spouse=4555931}
	7304.1.1 = {death="7304.1.1"}
}
4555931 = {
	name="Torma"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7244.1.1 = {birth="7244.1.1"}
	7304.1.1 = {death="7304.1.1"}
}
3655931 = {
	name="Ygon"	# not a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3555931
	mother=4555931

	7262.1.1 = {birth="7262.1.1"}
	7278.1.1 = {add_trait=skilled_warrior}
	7289.1.1 = {death="7289.1.1"}
}
3755931 = {
	name="Quort"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3555931
	mother=4555931

	7264.1.1 = {birth="7264.1.1"}
	7280.1.1 = {add_trait=trained_warrior}
	7282.1.1 = {add_spouse=4755931}
	7324.1.1 = {death="7324.1.1"}
}
4755931 = {
	name="Zia"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7264.1.1 = {birth="7264.1.1"}
	7324.1.1 = {death="7324.1.1"}
}
3855931 = {
	name="Drogg"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3755931
	mother=4755931

	7293.1.1 = {birth="7293.1.1"}
	7309.1.1 = {add_trait=master_warrior}
	7311.1.1 = {add_spouse=4855931}
	7332.1.1 = {death="7332.1.1"}
}
4855931 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7293.1.1 = {birth="7293.1.1"}
	7332.1.1 = {death="7332.1.1"}
}
3955931 = {
	name="Joth"	# a lord
	dynasty=55931

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7312.1.1 = {birth="7312.1.1"}
	7328.1.1 = {add_trait=poor_warrior}
	7330.1.1 = {add_spouse=4955931}
	7364.1.1 = {death="7364.1.1"}
}
4955931 = {
	name="Rowan"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7312.1.1 = {birth="7312.1.1"}
	7364.1.1 = {death="7364.1.1"}
}
40255931 = {
	name="Sogoloth"

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7313.1.1 = {birth="7313.1.1"}
	7329.1.1 = {add_trait=trained_warrior}
	7370.1.1 = {death="7370.1.1"}
}
41055931 = {
	name="Meralyia"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7314.1.1 = {birth="7314.1.1"}
	7354.1.1 = {death="7354.1.1"}
}
4255931 = {
	name="Jogg"

	religion="gods_ibben"
	culture="ibbenese"

	father=3855931
	mother=4855931

	7315.1.1 = {birth="7315.1.1"}
	7331.1.1 = {add_trait=trained_warrior}
	7369.1.1 = {death="7369.1.1"}
}
40355931 = {
	name="Frynne"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3955931
	mother=4955931

	7337.1.1 = {birth="7337.1.1"}
	7369.1.1 = {death="7369.1.1"}
}
4155931 = {
	name="Howd"

	religion="gods_ibben"
	culture="ibbenese"

	father=3955931
	mother=4955931

	7340.1.1 = {birth="7340.1.1"}
	7356.1.1 = {add_trait=poor_warrior}
	7358.1.1 = {add_spouse=41155931}
	7389.1.1 = {death="7389.1.1"}
}
41155931 = {
	name="Mellario"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7340.1.1 = {birth="7340.1.1"}
	7389.1.1 = {death="7389.1.1"}
}
42555931 = {
	name="Assadora"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=3955931
	mother=4955931

	7341.1.1 = {birth="7341.1.1"}
}
40555931 = {
	name="Eleogg"

	religion="gods_ibben"
	culture="ibbenese"

	father=4155931
	mother=41155931

	7367.1.1 = {birth="7367.1.1"}
	7383.1.1 = {add_trait=skilled_warrior}
	7385.1.1 = {add_spouse=40455931}
}
40455931 = {
	name="Betharios"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	7367.1.1 = {birth="7367.1.1"}
}
4055931 = {
	name="Bellegere"
	female=yes

	religion="gods_ibben"
	culture="ibbenese"

	father=40555931
	mother=40455931

	7400.1.1 = {birth="7400.1.1"}
}



