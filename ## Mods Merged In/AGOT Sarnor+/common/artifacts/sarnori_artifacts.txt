huzhor_amai_pelt = {
	quality = 3
	monthly_character_prestige = 0.2
	sarnorian_opinion = 5
	active = {
		is_adult = yes
		religion_group = sarnor_rel
	}
	picture = "GFX_huzhor_amai_pelt"
	indestructible = yes
	slot = ceremonial_torso
}